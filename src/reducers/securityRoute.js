import { SET_CURRENT_USER } from "../constants/actionTypes";

// import { SET_CURRENT_USER } from "../actions/types";

const initialState = {
  user: {},
  validToken: false
};

const booleanAcitonPayload = payload => {
  if (payload.username) {
    return true;
  } else {
    return false;
  }
};
export default function(state = initialState, action) {
  switch (action.type) {
    case SET_CURRENT_USER:
      return {
        ...state,
        validToken: booleanAcitonPayload(action.payload),
        user: action.payload
      };

    default:
      return state;
  }
}
