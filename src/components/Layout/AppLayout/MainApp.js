import React from "react";
import { Route, Switch } from "react-router-dom";
import loadable from "react-loadable";
import Header from "components/Layout/Header";
import Sidenav from "components/Layout/Sidenav";
import Footer from "components/Layout/Footer";
import Customizer from "components/Customizer";
import LoadingComponent from "components/Loading";
import SecureRoute from "../../../securityUnits/SecureRoute";
import "./styles.scss"
import "../../../routes/app/routes/routing/Home/components/style.scss"



let SalesDensityTrends = loadable({
  loader: () => import("routes/app/routes/routing/Sales Density Trends"),
  loading: LoadingComponent
});
let SalesForce = loadable({
  loader: () => import("routes/app/routes/routing/Sales Force Slicer"),
  loading: LoadingComponent
});
let Home = loadable({
  loader: () => import("routes/app/routes/routing/Home"),
  loading: LoadingComponent
});



class MainApp extends React.Component {
  render() {
    const { match } = this.props;

    return (
      <div className="main-app-container">
        <Sidenav />

        <section id="page-container" className="app-page-container">
          <Header />

          <div className="app-content-wrapper">
            <div className="app-content">
              <div className="h-100">
                <Switch>

                  <SecureRoute
                    path={`${match.url}/dashboard`}
                    component={Home}
                  />
                  <SecureRoute
                    path={`${match.url}/salesforce`}
                    component={SalesDensityTrends}
                  />
                  <SecureRoute
                    path={`${match.url}/salesforceslicer`}
                    component={SalesForce}
                  />
                 
                  
                </Switch>
              </div>
            </div>

            {/* <Footer /> */}
          </div>
        </section>

        {/* <Customizer /> */}
        {/* <Customizer /> Customization bar on right side */}
      </div>
    );
  }
}

export default MainApp;
