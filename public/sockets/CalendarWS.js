var calendar_ws;

var GetDateObject = {
  handle: 1,
  method: "GetObject",
  params: {
    qId: ""
  },
  outKey: -1,
  id: 2
};

var GetDateHyperCubeData = {
  handle: 2,
  method: "GetHyperCubeData",
  params: {
    qPath: "/qHyperCubeDef",
    qPages: [{ qTop: 0, qLeft: 0, qHeight: 10, qWidth: 10 }]
  },
  id: 3
};

function CalendarOpenWS() {
  var OpenDoc = {
    method: "OpenDoc",
    handle: -1,
    params: [
      appid
      //"8e3cb028-88d0-489d-923f-0e611eb7199d"
    ],
    outKey: -1,
    id: 1
  };

  calendar_ws = new WebSocket(
    "wss://" +
      config[env].host +
      config[env].prefix +
      "app/" +
      appid +
      "?reloadUri=https://" +
      config[env].host +
      config[env].prefix +
      "dev-hub/engine-api-explorer&v=" +
      new Date()
  );

  calendar_ws.onopen = function(msg) {
    // Logic for opened connection
    console.log("Date Range connection opened");
    calendar_ws.send(JSON.stringify(OpenDoc));
  };

  calendar_ws.onerror = function() {
    console.log("Error!!", "Some issues fetching date range");
  };
}

function LoadCalendarData(objId, callback) {
  GetDateObject.params.qId = objId;

  if (!calendar_ws) {
    CalendarOpenWS();
  }

  sendMessage(calendar_ws, JSON.stringify(GetDateObject));

  calendar_ws.onmessage = function(data, flags) {
    var wsres = JSON.parse(data.data);

    if (wsres.id == 2) {
      calendar_ws.send(JSON.stringify(GetDateHyperCubeData));
    }

    if (wsres.id == 3) {
      if (callback) {
        callback(wsres);
      }
    }
  };
}
