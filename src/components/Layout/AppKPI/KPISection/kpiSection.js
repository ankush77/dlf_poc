import React, { Component } from "react";
import HelpOutlineIcon from "@material-ui/icons/HelpOutline";
import "./styles.scss";

class KPISection extends Component {
  render() {
    return (
      

       <div className="col-md-12 padding10">
       <div className="col-md-12 KPI">
         <div className="row">
           <div className="col-md-2 KPIContent">
             <div className="row">
               <div className="col-md-12 KpiTitle whiteColor">
                 <div className="KPITitaleName">
                   Total apps{" "}
                   <span>
                     {" "}
                     <HelpOutlineIcon />
                   </span>
                 </div>
               </div>
               {/* <div className="col-md-2 whiteColor">
                  <HelpOutlineIcon/>
               </div> */}
               <div className="col-md-12 KpiData whiteColor">
                 <span className="ValueText">10</span>
               </div>
             </div>
           </div>
           <div className="col-md-5 KPIContent">
             <div className="row">
               <div className="col-md-12 KpiTitle whiteColor">
                 <div className="KPITitaleName">
                   New apps{" "}
                   <span>
                     {" "}
                     <HelpOutlineIcon />
                   </span>{" "}
                 </div>
               </div>
               <div className="col-md-12 KpiData whiteColor">
                 <div className="RecentDiv">
                   <div>
                     <a className="customAnchorLink" href="#">
                       Revenue Mashup
                     </a>
                   </div>
                   <div>
                     <span className="linkRedirect">27 Days ago</span>
                   </div>
                 </div>
                 <div className="RecentDiv">
                   <div>
                     <a className="customAnchorLink" href="#">
                       Revenue Mashup
                     </a>
                   </div>
                   <div>
                     <span className="linkRedirect">28 days ago</span>
                   </div>
                 </div>
               </div>
             </div>
           </div>
           <div className="col-md-5 KPIContent">
             <div className="row">
               <div className="col-md-12 KpiTitle whiteColor">
                 <div className="KPITitaleName">
                   Last data updates{" "}
                   <span>
                     {" "}
                     <HelpOutlineIcon />
                   </span>
                 </div>
               </div>
               <div className="col-md-12 KpiData whiteColor">
                 <div className="RecentDiv">
                   <div>
                     <a className="customAnchorLink" href="#">
                       Product Revenue App
                     </a>
                   </div>
                   <div>
                     <span className="linkRedirect">4 Months ago</span>
                   </div>
                 </div>
                 <div className="RecentDiv">
                   <div>
                     <a className="customAnchorLink" href="#">
                       Participation and Risk Extension Debug
                     </a>
                   </div>
                   <div>
                     <span className="linkRedirect">9 Months ago</span>
                   </div>
                 </div>
               </div>
             </div>
           </div>
         </div>
       </div>
     </div>

     
    );
  }
}

export default KPISection;
