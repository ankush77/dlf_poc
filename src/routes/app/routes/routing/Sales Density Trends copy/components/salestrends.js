import React from "react";
import QueueAnim from "rc-queue-anim";
import { Grid, Paper } from "@material-ui/core";
import "./styles.scss";
import $ from "jquery";
import Tooltip from "@material-ui/core/Tooltip";
import { Typography } from "@material-ui/core";
import { userActions } from "../../../../../../actions/userActions";
import { connect } from "react-redux";
const qlikConfig = require("./../../../../../../assets/configs/config.json");

class SalesDensityTrends extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    this.props.dispatch(userActions.getHeader(true));
    const options = qlikConfig;

    let count = 0;
    var checkExist = setInterval(function() {
      if (typeof window.LoadSalesDensityTrends === "function" || count > 30) {
        clearInterval(checkExist);
        window.LoadSalesDensityTrends();
      } else {
        setTimeout(function() {
          count++;
        }, 100);
      }
    }, 200);
  }
  render() {
    return (
      <div className="container-fluid no-breadcrumb page-dashboard">
       <div className="row backgroundDiv">
       
         <div className="col-md-3 col-sm-12 pr0">
            <div className="col-md-12 mb15 pd14 kpiBoxHeight whiteColor" id="salestrends_qv011">

            </div>
            <div className="col-md-12 mb15 pd14 kpiBoxHeight whiteColor"  id="salestrends_qv012" >

            </div>
            <div className="col-md-12  pd14 kpiBoxHeight whiteColor"  id="salestrends_qv013">

            </div>
         </div>
         <div className="col-md-6 col-sm-12" >
                <div className="col-md-12 mb15 pd14 kpiTitleHeight whiteColor" id="salestrends_qv014" >

                </div>
                <div className="col-md-12">
                <div className="row">
                <div className="col-md-6 pl0 pr7" >
                  <div className="col-md-12 pd14 kpiDataHeight whiteColor" id="salestrends_qv015"></div>
                  </div>
                <div className="col-md-6 pr0 pl7" >
                  <div className="col-md-12 pd14 kpiDataHeight whiteColor" id="salestrends_qv016" ></div>
                  </div>
                  </div>

                </div>
         </div>
         <div className="col-md-3 col-sm-12 pl0">
            <div className="col-md-12 mb15 pd14 kpiBoxHeight whiteColor" id="salestrends_qv017">

            </div>
            <div className="col-md-12 mb15 pd14 kpiBoxHeight whiteColor" id="salestrends_qv018">

            </div>
            <div className="col-md-12  pd14 kpiBoxHeight whiteColor" id="salestrends_qv019">

            </div>
         </div>
       </div>

        <Grid container spacing={16} style={{ paddingBottom: 20 }}>
          <Grid item sm lg={12}>
            <Paper>
              {/* <div className="row no-margin" style={{ paddingTop: "14px" }}>
                <div className="col-md-2" style={{ paddingLeft: "16px" }}>
                  <span className="strd-header">
                    Sales Density Monthly Heatmap
                </span>
                </div>
                <div className="col-md-8"></div>
                <div className="col-md-2">
                  <Tooltip
                    title={
                      <div>
                        <Typography
                          variant={"body1"}
                          style={{
                            color: "inherit",
                            fontfamily: "IrisCircular-medium"

                          }}
                        >
                          <div>
                            <strong />
                            The Monthly Sales Density Growth Rates are calculated as the Sales Density of the Respective Month divided by the Sales Density of the Same Month Last Year
                          </div>
                        </Typography>
                      </div>
                    }
                    placement="right-end"
                  >
                    <i class="material-icons md-dark info-icon-class">
                      info_outline
                    </i>
                  </Tooltip>
                </div>
              </div> */}
              <div
                className="remove-menu"
                id="salestrends_qv02"
                style={{ height: "400px", padding: "14px" }}
              />
            </Paper>
          </Grid>
        </Grid>

        <Grid container spacing={16} style={{ paddingBottom: 20 }}>
          <Grid item sm lg={12}>
            <Paper>
              {/* <div className="row no-margin" style={{ paddingTop: "14px" }}>
                <div className="col-md-2" style={{ paddingLeft: "16px" }}>
                  <span className="strd-header">
                    Sales Density Over Time Detail
                </span>
                </div>
                <div className="col-md-8"></div>
                <div className="col-md-2">
                  <Tooltip
                    title={
                      <div>
                        <Typography
                          variant={"body1"}
                          style={{
                            color: "inherit",
                            fontfamily: "IrisCircular-medium"

                          }}
                        >
                          <div>
                            <strong />
                            The Monthly Sales Density is calculated as the Sum of the Last 12 Months of Sales divided by the GLA (Cinema/Food Court/Retail) of the Respective Tenant(s)
                          </div>
                        </Typography>
                      </div>
                    }
                    placement="right-end"
                  >
                    <i class="material-icons md-dark info-icon-class">
                      info_outline
                    </i>
                  </Tooltip>
                </div>
              </div> */}
              <div
                className="remove-menu"
                id="salestrends_qv03"
                style={{ height: "400px", padding: "14px" }}
              />
            </Paper>
          </Grid>
        </Grid>
        <Grid container spacing={16} style={{ paddingBottom: 20 }}>
          <Grid item sm lg={12}>
            <Paper>
              <div
                className="remove-menu"
                id="salestrends_qv04"
                style={{ height: "400px", padding: "14px" }}
              />
            </Paper>
          </Grid>
        </Grid>
        {/* <div id="asset_filter" className="assetfilter" /> */}
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => ({
  showHeader: state.showHeader
});

export default connect(mapStateToProps)(SalesDensityTrends);
