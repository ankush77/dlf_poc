import {
  QLIK_SCRIPT_LOADED,
  MAIN_SCRIPT_LOADED,
  QLIK_OBJECT,
  QLIK_APP
} from "../constants/actionTypes";

export const qlikScriptsAction = {
  qlikScriptLoaded,
  mainScriptLoaded,
  getQlikInst,
  getQlikApp
};

function qlikScriptLoaded(status) {
  return dispatch => {
    // let user = response;
    dispatch(success(status));
  };

  function success(status) {
    return {
      type: QLIK_SCRIPT_LOADED,
      payload: status
    };
  }
}
function getQlikApp(app) {
  return dispatch => {
    // dispatch(success(app));
  };

  function success(app) {
    return {
      type: QLIK_APP,
      payload: app
    };
  }
}
function getQlikInst(myqlik) {
  return dispatch => {
    // let user = response;
    dispatch(success(myqlik));
  };

  function success(myqlik) {
    return {
      type: QLIK_OBJECT,
      payload: myqlik
    };
  }
}
function mainScriptLoaded(status) {
  return dispatch => {
    // let user = response;
    dispatch(success(status));
  };

  function success(status) {
    return {
      type: MAIN_SCRIPT_LOADED,
      payload: status
    };
  }
}
