const appid = "d1bfc8d2-ffc9-4c04-9594-f238f92d643a";
const qlikConfig = {
  appid: "d1bfc8d2-ffc9-4c04-9594-f238f92d643a",
  config: {
    baseUrl: "https://dev.instantview.io/in-view/resources",
    prefix: "/in-view/",
    port: 443,
    host: "dev.instantview.io",
    isSecure: true,
    config: {
      text: {
        useXhr: function(url, protocol, hostname, port) {
          // enable xhr load for all text resources
          return true;
        },
        createXhr: function() {
          // use withCredentials flag to prevent authentication errors
          var xhr = new XMLHttpRequest();
          xhr.withCredentials = true;
          return xhr;
        }
      }
    }
  }
};

export default qlikConfig;
