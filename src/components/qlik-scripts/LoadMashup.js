import React, { Component } from "react";
import Script from "react-load-script";
import { connect } from "react-redux";
import { qlikScriptsAction } from "./../../actions/qlikScriptsAction";
// import endpoints from "./../../assets/configs/endpoints";
// import env from "./../../assets/configs/environment";
class LoadMashup extends Component {
  constructor(props) {
    super(props);
    this.state = {
      qlikTicket: props.ticket
    };
    this.getQlikInstance = this.getQlikInstance.bind(this);
  }
  render() {
    const mainJs = "/main.js?v=" + new Date();
    return (
      <div>
        {this.props.qlikScripts.mainjs === "success"
          ? this.getQlikInstance()
          : ""}
        <Script
          url={mainJs}
          onCreate={this.handleScriptCreate.bind(this)}
          onError={this.handleScriptError.bind(this)}
          onLoad={this.handleScriptLoad.bind(this)}
          //   onLoad={this.props.loadQlik}
        />
      </div>
    );
  }

  getQlikInstance() {
    var that = this;
    var checkQlikLoaded = setInterval(function() {
      var myqlik = that.props.qlikScripts.qlik;

      if (window.myqlik && Object.entries(myqlik).length === 0) {
        console.log("get qlik");
        that.props.dispatch(qlikScriptsAction.getQlikInst(window.myqlik));
        // window.myqlik = null;

        // const app = window.myqlik.openApp(that.state.appid, that.state.config);
        // that.props.dispatch(qlikScriptsAction.getQlikApp(app));

        // window.myqlik.openApp();
        // https://dev.instantview.io/sense/app/d1bfc8d2-ffc9-4c04-9594-f238f92d643a
        clearInterval(checkQlikLoaded);
      } else {
        setTimeout(function() {}, 100);
      }
    }, 500);
  }

  handleScriptCreate() {
    // console.log("handleScriptCreate");
  }

  handleScriptError() {
    // console.log("handleScriptError");
    this.props.dispatch(qlikScriptsAction.mainScriptLoaded("failure"));
  }

  handleScriptLoad() {
    this.props.dispatch(qlikScriptsAction.mainScriptLoaded("success"));
    window.loadQlik();
    // console.log("handleScriptLoad");
  }
}

const mapStateToProps = (state, ownProps) => ({
  qlikScripts: state.qlikScripts
});

export default connect(mapStateToProps)(LoadMashup);
