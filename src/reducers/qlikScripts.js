import APPCONFIG from "constants/appConfig";
import {
  QLIK_SCRIPT_LOADED,
  MAIN_SCRIPT_LOADED,
  QLIK_OBJECT,
  QLIK_APP
} from "../constants/actionTypes";

const initialState = {
  requirejs: "",
  mainjs: "",
  qlik: {},
  app: {}
};

const qlikScripts = (state = initialState, action) => {
  // console.log(action)
  switch (action.type) {
    case QLIK_SCRIPT_LOADED:
      return {
        ...state,
        requirejs: action.payload
      };
    case MAIN_SCRIPT_LOADED:
      return {
        ...state,
        mainjs: action.payload
      };
    case QLIK_OBJECT:
      return {
        ...state,
        qlik: action.payload
      };
    case QLIK_APP:
      return {
        ...state,
        app: action.payload
      };

    default:
      return state;
  }
};

export default qlikScripts;
