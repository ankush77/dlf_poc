import React, { Component } from "react";
import InfoIcon from "@material-ui/icons/Info";
import Button from "@material-ui/core/Button";
import Tooltip from "@material-ui/core/Tooltip";

import "./styles.scss";

import { connect } from "react-redux";

class AppGrid extends Component {
    constructor(props) {
        super(props);
    }

    render() {

        return (
            <div className="col-md-3 padding15G">
                <div className="row margin0G">
                    <div className="col-md-12 col-sm-12 AppDataOneG" onClick={this.props.onClick}>
                        <div className="AppNameG">
                            <span>{this.props.appName}</span>
                        </div>
                    </div>
                    <div className="col-md-12 col-sm-12 StreamNameDivG">
                        <div className="AppStreamG">
                            <span>
                                {this.props.streamName == null
                                    ? "Stream Name"
                                    : this.props.streamName.name}
                            </span>
                        </div>
                    </div>

                </div>
            </div>
        );
    }
}

export default AppGrid;
