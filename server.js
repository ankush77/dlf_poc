const express = require("express");
const bodyParser = require("body-parser");
const path = require("path");
const app = express();
// var session = require("express-session");
const https = require("https");
// var auth = require("./server/lib/auth");
const fs = require("fs");
var router = express.Router();
const envConfig = require("./environment");
const configs = require("./configs");
const cors = require("cors");

// var favicon = require("serve-favicon");
// var logger = require("morgan");
// var cookieParser = require("cookie-parser");

var index = require("./server/routes/index");
var users = require("./server/routes/users");
var login = require("./server/routes/login");
const cors_sec = require("./server/bin/cross");

app.use(cors());
app.options("*", cors());

app.use(express.static(path.join(__dirname, "build")));

// app.use(logger("dev"));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
// app.use(cookieParser());
// app.use(express.static(path.join(__dirname, "server/public")));
app.use("/api/v1", cors_sec.CrossOriginHeaders);
// app.use(
//   session({
//     secret: "irispoc",
//     resave: true,
//     saveUninitialized: false
//   })
// );

//app.use('/api/v1/users', users);
// app.use("/api/v1/login", login);
// app.use("/api/v1/checkSession", login);

// app.use("/api/v1/data/", auth.requiresLogin, users);
app.use("/api/v1/data/", users);

// app.get("/ping", function(req, res) {
//   return res.send("pong");
// });

// router.get('/', auth.requiresLogin, function(req, res, next) {
//    // console.log(req.session.user)
//    res.render('home', {
//        user: req.session.user
//    });
// });

app.use(router);
require("./server/routes")(router);

app.get("/", function(req, res) {
  res.sendFile(path.join(__dirname, "build", "index.html"));
});

process.env["NODE_TLS_REJECT_UNAUTHORIZED"] = "0";

var credentials = {
  key: fs.readFileSync(path.resolve(__dirname, configs.ssl[envConfig.env].key)),
  cert: fs.readFileSync(path.resolve(__dirname, configs.ssl[envConfig.env].crt))
};

var httpsServer = https.createServer(credentials, app).listen(configs.appPort);
console.log("app is running on : " + configs.appPort);
