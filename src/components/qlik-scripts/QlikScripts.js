import React, { Component } from "react";
import Script from "react-load-script";
import { connect } from "react-redux";
import { qlikScriptsAction } from "./../../actions/qlikScriptsAction";

import qsConfig from "./../../assets/configs/endpoints";
const env = require("./../../assets/configs/environment").env;

class QlikScripts extends Component {
  constructor(props) {
    super(props);
    this.state = {
      qlikTicket: props.ticket,
      filterFlag: false,
      filterLoadedFlag: false
    };
  }
  render() {
    const requirejsUrl =
      qsConfig[env].qsconfigs.baseUrl +
      "/assets/external/requirejs/require.js?QlikTicket=" +
      this.state.qlikTicket; 
      // +
      // "&v=" +
      // new Date();
    return (
      <div>
        <Script
          url={requirejsUrl}
          onCreate={this.handleScriptCreate.bind(this)}
          onError={this.handleScriptError.bind(this)}
          onLoad={this.handleScriptLoad.bind(this)}
          //   onLoad={this.props.loadQlik}
        />
        {
          this.state.filterFlag && !this.state.filterLoadedFlag && (
            <Script url="https://qlikdemo.polestarllp.com/virproxy/extensions/qssidemenu/lib/partials/template.html" 
            onCreate={() => {this.setState({filterLoadedFlag: true})}}
            ></Script>
          )
        }
      </div>
    );
  }

  handleScriptCreate() {
    // console.log("handleScriptCreate");
  }

  handleScriptError() {
    // console.log("handleScriptError");
    this.props.dispatch(qlikScriptsAction.qlikScriptLoaded("failure"));
  }

  handleScriptLoad() {
    this.props.dispatch(qlikScriptsAction.qlikScriptLoaded("success"));
    // console.log("handleScriptLoad");
  // this.setState({filterFlag: true})
  }
}


const mapStateToProps = (state, ownProps) => ({
  qlikScripts: state.qlikScripts
});

export default connect(mapStateToProps)(QlikScripts);
