import React from "react";
import { withRouter } from "react-router-dom";
import Button from "@material-ui/core/Button";
import $ from "jquery";
import "jquery-slimscroll/jquery.slimscroll.min";
import DEMO from "constants/demoData";
import "./../../.././../node_modules/font-awesome/css/font-awesome.min.css";
import "./styles1.scss";
// import QdtComponents from 'qdt-components';
const qlikConfig = require("assets/configs/config.json");

// import { connect } from 'react-redux';
// const userList = require('./../../../assets/configs/userdata.json');
const options = qlikConfig;
// const QdtComponent = new QdtComponents(options.config, options.connections);

class SidebarContent extends React.Component {
  componentDidMount() {
    const { history, location } = this.props;
    const nav = this.nav;
    const $nav = $(nav);

    // scroll
    $nav.slimscroll({
      height: "100%"
    });

    // Append icon to submenu
    $nav
      .find(".prepend-icon")
      .prepend('<i class="material-icons">keyboard_arrow_right</i>');

    // AccordionNav
    const slideTime = 250;
    const $lists = $nav.find("ul").parent("li");
    $lists.append('<i class="material-icons icon-has-ul">arrow_drop_down</i>');
    const $As = $lists.children("a");

    // Disable A link that has ul
    $As.on("click", event => event.preventDefault());

    // Accordion nav
    $nav.on("click", e => {
      const target = e.target;
      const $parentLi = $(target).closest("li"); // closest, insead of parent, so it still works when click on i icons
      if (!$parentLi.length) return; // return if doesn't click on li
      const $subUl = $parentLi.children("ul");

      // let depth = $subUl.parents().length; // but some li has no sub ul, so...
      const depth = $parentLi.parents().length + 1;

      // filter out all elements (except target) at current depth or greater
      const allAtDepth = $nav.find("ul").filter(function() {
        if ($(this).parents().length >= depth && this !== $subUl.get(0)) {
          return true;
        }
        return false;
      });
      allAtDepth
        .slideUp(slideTime)
        .closest("li")
        .removeClass("open");

      // Toggle target
      if ($parentLi.has("ul").length) {
        $parentLi.toggleClass("open");
      }
      $subUl.stop().slideToggle(slideTime);
    });

    // HighlightActiveItems
    const $links = $nav.find("a");
    const currentLocation = history.location;
    function highlightActive(pathname) {
      const path = `#${pathname}`;

      $links.each((i, link) => {
        const $link = $(link);
        const $li = $link.parent("li");
        const href = $link.attr("href");
        // console.log(href);

        if ($li.hasClass("active")) {
          $li.removeClass("active");
        }
        if (path.indexOf(href) === 0) {
          $li.addClass("active");
        }
      });
    }
    highlightActive(currentLocation.pathname);
    history.listen(location => {
      highlightActive(location.pathname);
    });
  }

  SelectionHandler = () => {
    //QdtComponent.render('QdtCurrentSelections', {id:'',height:'100px'}, document.getElementById('qv61'));
  };

  homePageHandler = () => {
    setTimeout(function(){
      window.location.reload();
    },0.5)
  };
  // abc = () => {
   
  //     window.location.reload();

  // };

  render() {
    // const userList = JSON.parse(localStorage.getItem("user")).data;

    // let baseURL = userList[0].BASE_URL;
    // let pagesList = [];
    // let sectionName = [];
    // if (userList) {
    //   // for (var i = 0; i < userList.length; i++) {
    //   //   appplicationslist.push(userList[i].APP_NAME);
    //   // }
    //   // appplicationslist = Array.from(new Set(appplicationslist));

    //   // for (var i = 0; i < userList.length; i++) {
    //   //   clientsList.push(userList[i].CLIENT_NAME);
    //   // }
    //   // clientsList = Array.from(new Set(clientsList));

    //   for (var i = 0; i < userList.length; i++) {
    //     pagesList.push(userList[i].APP_PAGE_NAME);
    //   }
    //   pagesList = Array.from(new Set(pagesList));

    //   for (var i = 0; i < userList.length; i++) {
    //     sectionName.push(userList[i].APP_SECTION_NAME);
    //   }
    //   sectionName = Array.from(new Set(sectionName));
    // }

    return (
      <ul
        className="nav"
        ref={c => {
          this.nav = c;
        }}
      >
        {/* Homepage */}
        <li>
          <Button href="#/app/dashboard"  onClick={this.homePageHandler}>
            <i className="nav-icon material-icons">
              home
            </i>
            <span className="nav-text">Homepage</span>
          </Button>
        </li>

        <li>
          <Button href="#">
            <i className="nav-icon material-icons">web</i>
            <span className="nav-text">Sales</span>
          </Button>
          <ul>
            <li>
              <Button className="prepend-icon" href={"#/app/salesforceslicer"}>
                <span>Sales Force</span>
              </Button>
            </li>
          </ul>
        </li>

        <li className="nav-divider" />
      </ul>
    );
  }
}

export default withRouter(SidebarContent);

// const mapStateToProps = (state, ownProps) => ({
//   layoutBoxed: state.settings.layoutBoxed,
//   navCollapsed: state.settings.navCollapsed,
//   navBehind: state.settings.navBehind,
//   fixedHeader: state.settings.fixedHeader,
//   sidebarWidth: state.settings.sidebarWidth,
//   theme: state.settings.theme,
//   users: state.users

// });

// export default connect(
//   mapStateToProps
// )(SidebarContent);
