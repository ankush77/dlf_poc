import React from "react";
import PropTypes from "prop-types";
import classNames from "classnames";
import { withStyles } from "@material-ui/core/styles";
import Drawer from "@material-ui/core/Drawer";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import CssBaseline from "@material-ui/core/CssBaseline";
import List from "@material-ui/core/List";
import Typography from "@material-ui/core/Typography";
import Divider from "@material-ui/core/Divider";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import ChevronRightIcon from "@material-ui/icons/ChevronRight";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import InboxIcon from "@material-ui/icons/MoveToInbox";
import MailIcon from "@material-ui/icons/Mail";
import "./../styles1.scss";
const drawerWidth = 240;

const styles = theme => ({
  root: {
    display: "flex",
    width: "60px"
  },
  // appBarShift: {
  //   width: `calc(100% - ${drawerWidth}px)`,
  //   transition: theme.transitions.create(['margin', 'width'], {
  //     easing: theme.transitions.easing.easeOut,
  //     duration: theme.transitions.duration.enteringScreen,
  //   }),
  //   marginRight: drawerWidth,
  // },
  // menuButton: {
  //   marginLeft: 12,
  //   marginRight: 20,
  // },
  hide: {
    display: "none"
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0
  },
  drawerPaper: {
    width: drawerWidth
  },
  // drawerHeader: {
  //   display: 'flex',
  //   alignItems: 'center',
  //   padding: '0 8px',
  //   ...theme.mixins.toolbar,
  //   justifyContent: 'flex-start',
  // },
  content: {
    flexGrow: 1,
    padding: theme.spacing.unit * 3,
    transition: theme.transitions.create("margin", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen
    }),
    marginRight: -drawerWidth
  },
  contentShift: {
    transition: theme.transitions.create("margin", {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen
    }),
    marginRight: 0
  }
});

class FilterDrawerRight extends React.Component {
  state = {
    open: false,
    bgColor: "grey"
  };

  handleDrawerOpen = () => {
    this.setState({ open: true, bgColor: "#0099ff" });
    let count = 0;
  };

  handleDrawerClose = () => {
    this.setState({ open: false, bgColor: "grey" });
  };

  render() {
    const { classes, theme } = this.props;
    const { open } = this.state;

    return (
      <div className={classes.root} id="filters"
      style={{marginRight: '10px'}}>
        <CssBaseline />
        {/* <Toolbar disableGutters={!open}> */}
        <IconButton
          className="header-btn"
          color="inherit"
          aria-label="Open drawer"
          //onClick={this.handleDrawerOpen}
          style={{ color: this.state.bgColor ,width:'70px'}}
          id="filter-btn"
          // className={classNames(classes.menuButton, open && classes.hide)}
        >
          {/* <i className="material-icons filtricon">filter_list</i> */}
        </IconButton>
       
        {/* </Toolbar> */}

        <main
          className={classNames(classes.content, {
            [classes.contentShift]: open
          })}
        >
          {/* <div className={classes.drawerHeader} /> */}
        </main>
        {/* <Drawer
          className={classes.drawer}
          variant="persistent"
          anchor="right"
          open={open}
          classes={{
            paper: classes.drawerPaper
          }}
        >
          <div className={classes.drawerHeader}>
            <IconButton onClick={this.handleDrawerClose}>
              {theme.direction === "rtl" ? (
                <ChevronLeftIcon />
              ) : (
                <ChevronRightIcon />
              )}
            </IconButton>
          </div>

          <div
            id="filter-kfc"
            className="filters_list"
            style={{ height: "700px", zIndex:"1000" }}
          >
            <div class="loader" />
            <div class="loaderFullBackground" id="loading" />

            <div
              id="filter-kfc-pipeline2"
              className="video_dur filter_list"
              style={{ padding: "10px" }}
            />
            <div
              id="filter-kfc-pipeline1"
              className="video_dur filter_list"
              style={{ padding: "10px" }}
            />
            <div
              id="filter-kfc-pipeline6"
              className="video_dur filter_list"
              style={{ padding: "10px" }}
            />
            <div
              id="filter-kfc-pipeline3"
              className="video_dur filter_list"
              style={{ padding: "10px" }}
            />
            <div
              id="filter-kfc-pipeline5"
              className="video_dur filter_list"
              style={{ padding: "10px" }}
            />
            <div
              id="filter-kfc-pipeline4"
              className="video_dur filter_list"
              style={{ padding: "10px" }}
            />

            {/* <div className="video_dur padding-10 slider_list">
              <div className="ui sub header">Sliders</div>
              <div id="filter-kfc-pipeline-btn-1" className="height-55" />
            </div>

            <div className="video_dur padding-10 slider_list">
              <div className="ui sub header">LOT SIZE</div>
              <div
                id="filter-kfc-pipeline7"
                className="video_dur  "
                style={{
                  height: "60px",
                  width: "100%",
                  display: "block"
                }}
              />
            </div>

            <div className="video_dur padding-10 slider_list">
              <div className="ui sub header">NIY</div>
              <div
                id="filter-kfc-pipeline8"
                className="video_dur  "
                style={{
                  height: "60px",
                  width: "100%",
                  display: "block"
                }}
              />
            </div>
            <div className="video_dur padding-10 slider_list">
              <div className="ui sub header">OWNERSHIP %</div>
              <div
                id="filter-kfc-pipeline9"
                className="video_dur"
                style={{
                  height: "60px",
                  width: "100%",
                  display: "block"
                }}
              />
            </div> 

            <div
              id="filter-kfc-totalsales1"
              className="video_dur filter_list"
              style={{ padding: "10px" }}
            />
            <div
              id="filter-kfc-totalsales2"
              className="video_dur filter_list"
              style={{ padding: "10px" }}
            />
            <div
              id="filter-kfc-totalsales3"
              className="video_dur filter_list"
              style={{ padding: "10px" }}
            />
            <div
              id="filter-kfc-totalsales4"
              className="video_dur filter_list"
              style={{ padding: "10px" }}
            />
            <div
              id="filter-kfc-totalsales5"
              className="video_dur filter_list"
              style={{ padding: "10px" }}
            />
            <div
              id="filter-kfc-likeforlike1"
              className="video_dur filter_list"
              style={{ padding: "10px" }}
            />
            <div
              id="filter-kfc-likeforlike6"
              className="video_dur filter_list"
              style={{ padding: "10px" }}
            />
            <div
              id="filter-kfc-likeforlike7"
              className="video_dur filter_list"
              style={{ padding: "10px" }}
            />
            <div
              id="filter-kfc-likeforlike2"
              className="video_dur filter_list"
              style={{ padding: "10px" }}
            />
            <div
              id="filter-kfc-likeforlike8"
              className="video_dur filter_list"
              style={{ padding: "10px" }}
            />
            <div
              id="filter-kfc-likeforlike5"
              className="video_dur filter_list"
              style={{ padding: "10px" }}
            />
            <div
              id="filter-kfc-likeforlike9"
              className="video_dur filter_list"
              style={{ padding: "10px" }}
            />
            <div
              id="filter-kfc-likeforlike3"
              className="video_dur filter_list"
              style={{ padding: "10px" }}
            />
            <div
              id="filter-kfc-likeforlike4"
              className="video_dur filter_list"
              style={{ padding: "10px" }}
            />
            <div
              id="filter-kfc-likeforlike10"
              className="video_dur filter_list"
              style={{ padding: "10px" }}
            />
            <div
              id="filter-kfc-salesdensity5"
              className="video_dur filter_list"
              style={{ padding: "10px" }}
            />
            <div
              id="filter-kfc-salesdensity6"
              className="video_dur filter_list"
              style={{ padding: "10px" }}
            />
            <div
              id="filter-kfc-salesdensity7"
              className="video_dur filter_list"
              style={{ padding: "10px" }}
            />
            <div
              id="filter-kfc-salesdensity2"
              className="video_dur filter_list"
              style={{ padding: "10px" }}
            />
            <div
              id="filter-kfc-salesdensity8"
              className="video_dur filter_list"
              style={{ padding: "10px" }}
            />
            <div
              id="filter-kfc-salesdensity1"
              className="video_dur filter_list"
              style={{ padding: "10px" }}
            />
            <div
              id="filter-kfc-salesdensity9"
              className="video_dur filter_list"
              style={{ padding: "10px" }}
            />
            <div
              id="filter-kfc-salesdensity3"
              className="video_dur filter_list"
              style={{ padding: "10px" }}
            />
            <div
              id="filter-kfc-salesdensity4"
              className="video_dur filter_list"
              style={{ padding: "10px" }}
            />
            <div
              id="filter-kfc-salesdensity10"
              className="video_dur filter_list"
              style={{ padding: "10px" }}
            />
            <div
              id="filter-kfc-nonsales8"
              className="video_dur filter_list"
              style={{ padding: "10px" }}
            />
            <div
              id="filter-kfc-nonsales9"
              className="video_dur filter_list"
              style={{ padding: "10px" }}
            />
            <div
              id="filter-kfc-nonsales10"
              className="video_dur filter_list"
              style={{ padding: "10px" }}
            />
            <div
              id="filter-kfc-nonsales3"
              className="video_dur filter_list"
              style={{ padding: "10px" }}
            />
            <div
              id="filter-kfc-nonsales11"
              className="video_dur filter_list"
              style={{ padding: "10px" }}
            />
            <div
              id="filter-kfc-nonsales2"
              className="video_dur filter_list"
              style={{ padding: "10px" }}
            />
            <div
              id="filter-kfc-nonsales12"
              className="video_dur filter_list"
              style={{ padding: "10px" }}
            />
            <div
              id="filter-kfc-nonsales1"
              className="video_dur filter_list"
              style={{ padding: "10px" }}
            />
            <div
              id="filter-kfc-nonsales4"
              className="video_dur filter_list"
              style={{ padding: "10px" }}
            />
            <div
              id="filter-kfc-nonsales13"
              className="video_dur filter_list"
              style={{ padding: "10px" }}
            />
            
            {/* <div
              id="filter-kfc-nonsales5"
              className="video_dur filter_list"
              style={{ padding: "10px" }}
            />
            <div
              id="filter-kfc-nonsales6"
              className="video_dur filter_list"
              style={{ padding: "10px" }}
            />
            <div
              id="filter-kfc-nonsales7"
              className="video_dur filter_list"
              style={{ padding: "10px" }}
            /> 
            <div
              id="filter-kfc-nonsales14"
              className="video_dur filter_list"
              style={{ padding: "10px" }}
            />
            <div
              id="filter-kfc-ocr5"
              className="video_dur filter_list"
              style={{ padding: "10px" }}
            />
            <div
              id="filter-kfc-ocr6"
              className="video_dur filter_list"
              style={{ padding: "10px" }}
            />
            <div
              id="filter-kfc-ocr7"
              className="video_dur filter_list"
              style={{ padding: "10px" }}
            />
            <div
              id="filter-kfc-ocr4"
              className="video_dur filter_list"
              style={{ padding: "10px" }}
            />
            <div
              id="filter-kfc-ocr8"
              className="video_dur filter_list"
              style={{ padding: "10px" }}
            />
             <div
              id="filter-kfc-ocr3"
              className="video_dur filter_list"
              style={{ padding: "10px" }}
            />
             <div
              id="filter-kfc-ocr9"
              className="video_dur filter_list"
              style={{ padding: "10px" }}
            />
            <div
              id="filter-kfc-ocr1"
              className="video_dur filter_list"
              style={{ padding: "10px" }}
            />
            <div
              id="filter-kfc-ocr2"
              className="video_dur filter_list"
              style={{ padding: "10px" }}
            />
            <div
              id="filter-kfc-ocr10"
              className="video_dur filter_list"
              style={{ padding: "10px" }}
            />
           
            <div
              id="filter-kfc-performance9"
              className="video_dur filter_list"
              style={{ padding: "10px" }}
            />
            <div
              id="filter-kfc-performance10"
              className="video_dur filter_list"
              style={{ padding: "10px" }}
            />
            <div
              id="filter-kfc-performance11"
              className="video_dur filter_list"
              style={{ padding: "10px" }}
            />
            <div
              id="filter-kfc-performance1"
              className="video_dur filter_list"
              style={{ padding: "10px" }}
            />
             <div
              id="filter-kfc-performance8"
              className="video_dur filter_list"
              style={{ padding: "10px" }}
            />
             <div
              id="filter-kfc-performance4"
              className="video_dur filter_list"
              style={{ padding: "10px" }}
            />
             <div
              id="filter-kfc-performance12"
              className="video_dur filter_list"
              style={{ padding: "10px" }}
            />
            <div
              id="filter-kfc-performance2"
              className="video_dur filter_list"
              style={{ padding: "10px" }}
            />
                       
            <div
              id="filter-kfc-performance3"
              className="video_dur filter_list"
              style={{ padding: "10px" }}
            />
            <div
              id="filter-kfc-performance13"
              className="video_dur filter_list"
              style={{ padding: "10px" }}
            />
            <div
              id="filter-kfc-performance14"
              className="video_dur filter_list"
              style={{ padding: "10px" }}
            />
            <div
              id="filter-kfc-performance15"
              className="video_dur filter_list"
              style={{ padding: "10px" }}
            />
            
            {/* <div
              id="filter-kfc-performance5"
              className="video_dur filter_list"
              style={{ padding: "10px" }}
            />
            <div
              id="filter-kfc-performance6"
              className="video_dur filter_list"
              style={{ padding: "10px" }}
            />
            <div
              id="filter-kfc-performance7"
              className="video_dur filter_list"
              style={{ padding: "10px" }}
            /> 
           
            <div
              id="filter-kfc-footfalls1"
              className="video_dur filter_list"
              style={{ padding: "10px" }}
            />
            <div
              id="filter-kfc-expiry10"
              className="video_dur filter_list"
              style={{ padding: "10px" }}
            />
            <div
              id="filter-kfc-expiry11"
              className="video_dur filter_list"
              style={{ padding: "10px" }}
            />
            <div
              id="filter-kfc-expiry12"
              className="video_dur filter_list"
              style={{ padding: "10px" }}
            />
            <div
              id="filter-kfc-expiry9"
              className="video_dur filter_list"
              style={{ padding: "10px" }}
            />
            <div
              id="filter-kfc-expiry13"
              className="video_dur filter_list"
              style={{ padding: "10px" }}
            />
            <div
              id="filter-kfc-expiry2"
              className="video_dur filter_list"
              style={{ padding: "10px" }}
            />
            <div
              id="filter-kfc-expiry14"
              className="video_dur filter_list"
              style={{ padding: "10px" }}
            />
            <div
              id="filter-kfc-expiry1"
              className="video_dur filter_list"
              style={{ padding: "10px" }}
            />
            <div
              id="filter-kfc-expiry15"
              className="video_dur filter_list"
              style={{ padding: "10px" }}
            />
            <div
              id="filter-kfc-expiry16"
              className="video_dur filter_list"
              style={{ padding: "10px" }}
            />
            <div
              id="filter-kfc-expiry3"
              className="video_dur filter_list"
              style={{ padding: "10px" }}
            />
            <div
              id="filter-kfc-expiry4"
              className="video_dur filter_list"
              style={{ padding: "10px" }}
            />
            <div
              id="filter-kfc-expiry5"
              className="video_dur filter_list"
              style={{ padding: "10px" }}
            />
            <div
              id="filter-kfc-expiry6"
              className="video_dur filter_list"
              style={{ padding: "10px" }}
            />
            <div
              id="filter-kfc-expiry7"
              className="video_dur filter_list"
              style={{ padding: "10px" }}
            />
            <div
              id="filter-kfc-expiry8"
              className="video_dur filter_list"
              style={{ padding: "10px" }}
            />
            <div
              id="filter-kfc-overview5"
              className="video_dur filter_list"
              style={{ padding: "10px" }}
            />
            <div
              id="filter-kfc-overview6"
              className="video_dur filter_list"
              style={{ padding: "10px" }}
            />
            <div
              id="filter-kfc-overview7"
              className="video_dur filter_list"
              style={{ padding: "10px" }}
            />
            <div
              id="filter-kfc-overview1"
              className="video_dur filter_list"
              style={{ padding: "10px" }}
            />
            <div
              id="filter-kfc-overview8"
              className="video_dur filter_list"
              style={{ padding: "10px" }}
            />
            <div
              id="filter-kfc-overview2"
              className="video_dur filter_list"
              style={{ padding: "10px" }}
            />
            <div
              id="filter-kfc-overview9"
              className="video_dur filter_list"
              style={{ padding: "10px" }}
            />
            <div
              id="filter-kfc-overview3"
              className="video_dur filter_list"
              style={{ padding: "10px" }}
            />
            <div
              id="filter-kfc-overview10"
              className="video_dur filter_list"
              style={{ padding: "10px" }}
            />
            <div
              id="filter-kfc-overview4"
              className="video_dur filter_list"
              style={{ padding: "10px" }}
            />
            <div
              id="filter-kfc-tenancy_01"
              className="video_dur filter_list"
              style={{ padding: "10px" }}
            />
            <div
              id="filter-kfc-tenantdash_01"
              className="video_dur filter_list"
              style={{ padding: "10px" }}
            />
          </div>
          {/* <div>
            <select name="category" id="filter-kfc1" className="video_dur filter_list" style={{width:"90%",height:"30px"}}>
              <option value="" disabled selected>Select</option>
            </select>
          </div>

           <div>
            <select name="hostname" id="filter-kfc2" className="select_brand filter_list" style={{width:"90%",height:"30px"}}>
              <option value="" disabled selected>Select</option>
            </select>
          </div> 
		  
          <div>
            <select name="category" id="filter-kfc3" className="video_dur filter_list" style={{width:"90%",height:"30px"}}>
              <option value="" disabled selected>Select</option>
            </select>
          </div>

          <div>
            <select name="category" id="filter-kfc4" className="video_dur filter_list" style={{width:"90%",height:"30px"}}>
              <option value="" disabled selected>Select</option>
            </select>
          </div>

          <div>
            <select name="category" id="filter-kfc5" className="video_dur filter_list" style={{width:"90%",height:"30px"}}>
              <option value="" disabled selected>Select</option>
            </select>
          </div>

          <div>
            <select name="category" id="filter-kfc6" className="video_dur filter_list" style={{width:"90%",height:"30px"}}>
              <option value="" disabled selected>Select</option>
            </select>
          </div>

          <div>
            <select name="category" id="filter-kfc7" className="video_dur filter_list" style={{width:"90%",height:"30px"}}>
              <option value="" disabled selected>Select</option>
            </select>
          </div> 

          <div
            id="filter-date_range"
            className="filters_daterange"
            style={{ height: "200px" }}
          />
          {/* <div id="filter-social-overview" style={{ height: "200px"}}></div>
          <div id="filter-competitior-overview" style={{ height: "200px"}}></div>
          <div id="filter-web" style={{ height: "200px"}}></div>
          <div id="filter-brand-pulse" style={{ height: "200px"}}></div>
          <div id="filter-facebook" style={{ height: "200px"}}></div>
          <div id="filter-youtube" style={{ height: "200px"}}></div>
          <div id="filter-twitter" style={{ height: "200px"}}></div>
          <div id="filter-instagram" style={{ height: "200px"}}></div> */}

          {/* <div id="filter-div" style={{ height: "200px"}}></div>
          <div id="filter-div2" style={{ height: "300px"}}></div>
          <div id="filter-div3" style={{ height: "250px"}}></div>
          <div id="filter-div4" style={{ height: "550px"}}></div>
          <div id="filter-div5" style={{ height: "200px"}}></div>
          <div id="filter-div6" style={{ height: "200px"}}></div> */}
          {/* <div id="filter-div7" style={{ height: "200px"}}></div> */}
          {/* <div id="filter-div" style={{ height: "250px"}}></div> samsung filters */}
          {/* <div id="filter-div2" style={{ height: "100px"}}></div> samsung filters */}
          {/* <div id="filter-div3" style={{ height: "200px"}}></div> samsung filters 
        </Drawer> */}
      </div>
    );
  }
}

FilterDrawerRight.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired
};

export default withStyles(styles, { withTheme: true })(FilterDrawerRight);

// export default withRouter(Calendar);
