import React from "react";
import APPCONFIG from "constants/appConfig";
import QueueAnim from "rc-queue-anim";
import { connect } from "react-redux";
import { userActions } from "../../../../../actions/userActions";
import "./sign-up.css";
import Button from "@material-ui/core/Button";
import Input from "@material-ui/core/Input";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import classnames from "classnames";

// const HorizontalForm = () => (
//   <article className="article">
//     <div className="box box-default">
//       <div className="reg-message">Add New User</div>
//       <div className="box-body py-5">

//         <form action="/examples/actions/confirmation.php" method="post" onSubmit={this.onSubmit}>
//           <div className="form-group row">
//             <label htmlFor="inputName2" className="col-md-2 col-form-label form_labels">Username</label>
//             <div className="col-md-10">
//               <Input
//                 placeholder="Username"
//                 id="inputName2"
//                 value={this.state.value}
//                 onChange={this.onChange}
//                 fullWidth
//               />
//             </div>
//           </div>
//           <div className="form-group row">
//             <label htmlFor="inputName3" className="col-md-2 col-form-label form_labels">Fullname</label>
//             <div className="col-md-10">
//               <Input
//                 placeholder="Fullname"
//                 id="inputName3"
//                 value={this.state.value}
//                 onChange={this.onChange}
//                 fullWidth
//               />
//             </div>
//           </div>
//           <div className="form-group row">
//             <label htmlFor="inputName4" className="col-md-2 col-form-label form_labels">Qlikuser</label>
//             <div className="col-md-10">
//               <Input
//                 placeholder="Qlikuser"
//                 id="inputName4"
//                 value={this.state.value}
//                 onChange={this.onChange}
//                 fullWidth
//               />
//             </div>
//           </div>
//           <div className="form-group row">
//             <label htmlFor="inputName5" className="col-md-2 col-form-label form_labels">Qlikdirectory</label>
//             <div className="col-md-10">
//               <Input
//                 placeholder="Qlikdirectory"
//                 id="inputName5"
//                 value={this.state.value}
//                 onChange={this.onChange}
//                 fullWidth
//               />
//             </div>
//           </div>
//           <div className="form-group row">
//             <label htmlFor="inputPassword2" className="col-md-2 col-form-label form_labels">Password</label>
//             <div className="col-md-10">
//               <Input
//                 placeholder="Password"
//                 id="inputPassword2"
//                 value={this.state.value}
//                 onChange={this.onChange}
//                 fullWidth
//               />
//             </div>
//           </div>
//           <div className="form-group row">
//             <label htmlFor="inputPassword3" className="col-md-2 col-form-label form_labels">Confirm Password</label>
//             <div className="col-md-10">
//               <Input
//                 placeholder="Confirm Password"
//                 id="inputPassword3"
//                 value={this.state.value}
//                 onChange={this.onChange}
//                 fullWidth
//               />
//             </div>
//           </div>
//           <div className="row">
//             <div className="col-md-2"></div>
//             <div className="col-md-10 form_labels">
//               <FormControlLabel
//                 control={
//                   <Checkbox
//                     value="checked"
//                     color="primary"
//                     onChange={this.onChange}
//                   />
//                 }
//                 label="Status"
//               />
//             </div>
//           </div>
//           <div className="form-group row">
//             <div className="col-md-2"></div>
//             <div className="col-md-10">
//               <Button variant="contained" className="btn-w-md form_labels"> Submit </Button>
//             </div>
//           </div>
//         </form>

//       </div>
//     </div>
//   </article>
// );

class SignUp extends React.Component {
  constructor() {
    super();
    this.state = {
      brand: APPCONFIG.brand,
      username: "",
      fullname: "",
      qlikuser: "",
      qlikdirectory: "",
      password: "",
      confirmPassword: "",
      status: true,
      errors: {},
      loading: false
    };
    this.onSubmit = this.onSubmit.bind(this);
    this.onChange = this.onChange.bind(this);
  }

  onSubmit(e) {
    e.preventDefault();
    const RegisterRequest = {
      username: this.state.username,
      fullname: this.state.fullname,
      qlikUser: {
        username: this.state.qlikuser,
        userdirectory: this.state.qlikdirectory
      },
      password: this.state.password,
      confirmPassword: this.state.confirmPassword,
      status: this.state.status
    };

    if (this.state.password === this.state.confirmPassword) {
      this.props.dispatch(userActions.register(RegisterRequest));
    } else {
      this.setState({
        errors: {
          confirmPassword: "Password must be same"
        }
      });
      // alert("Password must same");
    }
  }
  onChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.errors) {
      this.setState({ errors: nextProps.errors });
    }
  }

  render() {
    const { errors } = this.state;
    return (
      <div>
        <article className="article">
          <div className="box box-default mainBox">
            <div className="reg-message">Add New User</div>
            <div className="box-body">
              <form
                action="/examples/actions/confirmation.php"
                method="post"
                onSubmit={this.onSubmit}
              >
                <div className="form-group row">
                  <label
                    htmlFor="inputName2"
                    className="col-md-2 col-form-label form_labels"
                  >
                    Username
                  </label>
                  <div className="col-md-10">
                    <Input
                      placeholder="Username"
                      id="inputName2"
                      name="username"
                      value={this.state.value}
                      onChange={this.onChange}
                      fullWidth
                    />
                    {errors.username && (
                      <div className="invalid-feedback">{errors.username}</div>
                    )}
                  </div>
                </div>
                <div className="form-group row">
                  <label
                    htmlFor="inputName3"
                    className="col-md-2 col-form-label form_labels"
                  >
                    Fullname
                  </label>
                  <div className="col-md-10">
                    <Input
                      placeholder="Fullname"
                      id="inputName3"
                      name="fullname"
                      value={this.state.value}
                      onChange={this.onChange}
                      fullWidth
                    />
                    {errors.fullname && (
                      <div className="invalid-feedback">{errors.fullname}</div>
                    )}
                  </div>
                </div>
                <div className="form-group row">
                  <label
                    htmlFor="inputName4"
                    className="col-md-2 col-form-label form_labels"
                  >
                    Qlikuser
                  </label>
                  <div className="col-md-10">
                    <Input
                      placeholder="Qlikuser"
                      id="inputName4"
                      name="qlikuser"
                      value={this.state.value}
                      onChange={this.onChange}
                      fullWidth
                    />
                    {errors.qlikuser && (
                      <div className="invalid-feedback">{errors.qlikuser}</div>
                    )}
                  </div>
                </div>
                <div className="form-group row">
                  <label
                    htmlFor="inputName5"
                    className="col-md-2 col-form-label form_labels"
                  >
                    Qlikdirectory
                  </label>
                  <div className="col-md-10">
                    <Input
                      placeholder="Qlikdirectory"
                      id="inputName5"
                      name="qlikdirectory"
                      value={this.state.value}
                      onChange={this.onChange}
                      fullWidth
                    />
                    {errors.qlikdirectory && (
                      <div className="invalid-feedback">
                        {errors.qlikdirectory}
                      </div>
                    )}
                  </div>
                </div>
                <div className="form-group row">
                  <label
                    htmlFor="inputPassword2"
                    className="col-md-2 col-form-label form_labels"
                  >
                    Password
                  </label>
                  <div className="col-md-10">
                    <Input
                      placeholder="Password"
                      id="inputPassword2"
                      name="password"
                      value={this.state.value}
                      className="height-6vh"
                      type="password"
                      onChange={this.onChange}
                      fullWidth
                    />

                    {errors.password && (
                      <div className="invalid-feedback">{errors.password}</div>
                    )}
                  </div>
                </div>
                <div className="form-group row">
                  <label
                    htmlFor="inputPassword3"
                    className="col-md-2 col-form-label form_labels"
                  >
                    Confirm Password
                  </label>
                  <div className="col-md-10">
                    <Input
                      placeholder="Confirm Password"
                      id="inputPassword3"
                      name="confirmPassword"
                      className="height-6vh"
                      type="password"
                      value={this.state.value}
                      onChange={this.onChange}
                      fullWidth
                    />
                    {errors.confirmPassword && (
                      <div className="invalid-feedback">
                        {errors.confirmPassword}
                      </div>
                    )}
                  </div>
                </div>
                <div className="form-group row submitBtn">
                  <div className="col-md-2" />
                  <div className="col-md-10">
                    <Button
                      variant="contained"
                      className="btn-w-md form_labels"
                      onClick={this.onSubmit}
                    >
                      Submit
                    </Button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </article>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    users: state.users,
    errors: state.errors
  };
};
export default connect(mapStateToProps)(SignUp);
