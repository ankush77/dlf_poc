var Funnel_ws;

var GetDateObject = {
  handle: 1,
  method: "GetObject",
  params: {
    qId: ""
  },
  outKey: -1,
  id: 2
};

var GetDateHyperCubeData = {
  handle: 2,
  method: "GetHyperCubeData",
  params: {
    qPath: "/qHyperCubeDef",
    qPages: [{ qTop: 0, qLeft: 0, qHeight: 10, qWidth: 10 }]
  },
  id: 3
};

function FunnelOpenWS() {
  var OpenDoc = {
    method: "OpenDoc",
    handle: -1,
    params: [
      appid
      //"8e3cb028-88d0-489d-923f-0e611eb7199d"
    ],
    outKey: -1,
    id: 1
  };

  Funnel_ws = new WebSocket(
    "wss://" +
      config[env].host +
      config[env].prefix +
      "app/" +
      appid +
      "?reloadUri=https://" +
      config[env].host +
      config[env].prefix +
      "dev-hub/engine-api-explorer&v=" +
      new Date()
  );

  Funnel_ws.onopen = function(msg) {
    // Logic for opened connection
    console.log("Date Range connection opened");
    Funnel_ws.send(JSON.stringify(OpenDoc));
  };

  Funnel_ws.onerror = function() {
    console.log("Error!!", "Some issues fetching date range");
  };
}

function LoadFunnelData(objId, callback) {
  GetDateObject.params.qId = objId;

  if (!Funnel_ws) {
    FunnelOpenWS();
  }

  sendMessage(Funnel_ws, JSON.stringify(GetDateObject));

  Funnel_ws.onmessage = function(data, flags) {
    var wsres = JSON.parse(data.data);

    if (wsres.id == 2) {
      Funnel_ws.send(JSON.stringify(GetDateHyperCubeData));
    }

    if (wsres.id == 3) {
      if (callback) {
        callback(wsres);
      }
    }
  };
}
