var ws = new WebSocket(
  "wss://" +
    config[env].host +
    config[env].prefix +
    "app/" +
    appid +
    "?reloadUri=https://" +
    config[env].host +
    config[env].prefix +
    "dev-hub/engine-api-explorer"
);

var OpenDoc = {
  method: "OpenDoc",
  handle: -1,
  params: [
    appid
    //"8e3cb028-88d0-489d-923f-0e611eb7199d"
  ],
  outKey: -1,
  id: 1
};
var GetFieldIdInitial = 200000,
  GetObjectIdInitial = 300000,
  GetLayoutIdInitial = 400000,
  GetHyperCubeIdInitial = 500000,
  GetListObjectDataIdInitial = 600000,
  SelectValuesIdInitial = 700000,
  GetLayoutListBoxIdInitial = 800000,
  GetDateRangeIdInitial = 900000,
  GetDateRangeHypercubeIdInitial = 1000000;
var handle1 = 1;
var handle2;
var handle3;
var handle4;
var layout = {};
var idInformation = {};
var divIds = [];
var handleOfIds = [];
var response;
var callBack;
var callBackdata2;
var callBackk;
var callBackdata1;
var ObjId;
var SelectedValues;
ws.onmessage = function(event) {
  response = JSON.parse(event.data);
  // console.log(response);

  if (response.method == "OnConnected") {
    ws.send(JSON.stringify(OpenDoc));
  } else if (response.method == "OnAuthenticationInformation") {
    console.log("Authentication Done");
  } else if (response.id == 1 && response.result) {
    var handle = response.result.qReturn.qHandle;
    handle1 = handle;
  } else if (response.id > 200000 && response.id < 300000 && response.result) {
    var handlee = response.result.qReturn.qHandle;
    var id = ++SelectValuesIdInitial;
    idInformation[id] = {};
    idInformation[id].handle = handlee;
    idInformation[id].callBack = idInformation[response.id].callBack;
    idInformation[id].selectedvalues =
      idInformation[response.id].selectedvalues;
    selectvalues(handlee, idInformation[id].selectedvalues, id);
  } else if (response.id > 300000 && response.id < 400000 && response.result) {
    if (response.result.qReturn.qGenericType == "listbox") {
      var handle = response.result.qReturn.qHandle;
      var id = ++GetListObjectDataIdInitial;
      idInformation[id] = {};
      idInformation[id].divId = idInformation[response.id].divId;
      idInformation[id].callBack = idInformation[response.id].callBack;
      idInformation[id].handle = handle;
      GetListObjectData(handle, id);
    } else if (response.result.qReturn.qGenericType == "filterpane") {
      var handle = response.result.qReturn.qHandle;
      var id = ++GetLayoutListBoxIdInitial;
      idInformation[id] = {};
      idInformation[id].divId = idInformation[response.id].divId;
      idInformation[id].callBack = idInformation[response.id].callBack;
      idInformation[id].handle = handle;
      getObjectLayout2(handle, id);
    }
  } else if (response.id > 600000 && response.id < 700000) {
    var id = idInformation[response.id].divId;
    var callBack = idInformation[response.id].callBack;
    layout.objectdata = response;
    layout.id = id;
    if (callBack) {
      callBack(layout);
    }
  } else if (response.id > 800000 && response.id < 900000) {
    var id = idInformation[response.id].divId;
    var callBack = idInformation[response.id].callBack;
    layout.header2 = response;
    layout.id = id;
    if (callBack) {
      callBack(layout);
    }
  }
};

function GetData1(objid, divId, callback) {
  //callBackdata1 = callback;
  ObjId = objid;
  var id = ++GetObjectIdInitial;
  idInformation[id] = {};
  idInformation[id].divId = divId;
  idInformation[id].handle = handle1;
  idInformation[id].callBack = callback;
  getObjectdata(handle1, objid, id);
}

function GetData2(objid, fltrId, callback) {
  //callBackdata2 = callback;
  ObjId = objid;
  var id = ++GetObjectIdInitial;
  idInformation[id] = {};
  idInformation[id].divId = fltrId;
  idInformation[id].handle = handle1;
  idInformation[id].callBack = callback;
  getObjectdata(handle1, objid, id);
}

function getObjectdata(handle, objid, id) {
  var GetObject = {
    handle: handle,
    method: "GetObject",
    params: {
      qId: objid
    },
    outKey: -1,
    id: id
  };
  ws.send(JSON.stringify(GetObject));
}

function getObjectLayout2(handle, id) {
  var GetLayout2 = {
    handle: handle,
    method: "GetLayout",
    params: {},
    outKey: -1,
    id: id
  };
  ws.send(JSON.stringify(GetLayout2));
}

function getObjectHyperCubeData(handle, id) {
  var GetHyperCubeData = {
    handle: handle,
    method: "GetHyperCubeData",
    params: {
      qPath: "/qHyperCubeDef",
      qPages: [{ qTop: 0, qLeft: 0, qHeight: 10, qWidth: 10 }]
    },
    id: id
  };

  ws.send(JSON.stringify(GetHyperCubeData));
}

function GetListObjectData(handle, id) {
  var GetListObjectData = {
    handle: handle,
    method: "GetListObjectData",
    params: {
      qPath: "/qListObjectDef",
      qPages: [{ qTop: 0, qLeft: 0, qHeight: 1000, qWidth: 10 }]
    },
    id: id
  };
  ws.send(JSON.stringify(GetListObjectData));
}
