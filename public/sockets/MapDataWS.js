var map_link_ws;

var GetMapObject = {
  handle: 1,
  method: "GetObject",
  params: {
    qId: ""
  },
  outKey: -1,
  id: 2
};

var GetMapHyperCubeData = {
  handle: 2,
  method: "GetHyperCubeData",
  params: {
    qPath: "/qHyperCubeDef",
    qPages: [{ qTop: 0, qLeft: 0, qHeight: 10, qWidth: 10 }]
  },
  id: 3
};

function MapLinkOpenWS() {
  var OpenDoc = {
    method: "OpenDoc",
    handle: -1,
    params: [
      appid
      //"8e3cb028-88d0-489d-923f-0e611eb7199d"
    ],
    outKey: -1,
    id: 1
  };

  map_link_ws = new WebSocket(
    "wss://" +
      config[env].host +
      config[env].prefix +
      "app/" +
      appid +
      "?reloadUri=https://" +
      config[env].host +
      config[env].prefix +
      "dev-hub/engine-api-explorer&v=" +
      new Date()
  );

  map_link_ws.onopen = function(msg) {
    // Logic for opened connection
    console.log("Date Range connection opened");
    map_link_ws.send(JSON.stringify(OpenDoc));
  };

  map_link_ws.onerror = function() {
    console.log("Error!!", "Some issues fetching date range");
  };
}

function LoadMapLinkData(objId, callback) {
  GetMapObject.params.qId = objId;

  if (!map_link_ws) {
    MapLinkOpenWS();
  }

  sendMessage(map_link_ws, JSON.stringify(GetMapObject));

  map_link_ws.onmessage = function(data, flags) {
    var wsres = JSON.parse(data.data);

    if (wsres.id == 2) {
      map_link_ws.send(JSON.stringify(GetMapHyperCubeData));
    }

    if (wsres.id == 3) {
      if (callback) {
        callback(wsres);
      }
    }
  };
}
