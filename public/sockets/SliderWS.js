var sliders_ws;

var GetDateObject = {
  handle: 1,
  method: "GetObject",
  params: {
    qId: ""
  },
  outKey: -1,
  id: 2
};

var GetLayoutData = {
  handle: 2,
  method: "GetLayout",
    params: {},
  id: 3
};

function SliderOpenWS() {
  var OpenDoc = {
    method: "OpenDoc",
    handle: -1,
    params: [
      appid
      //"8e3cb028-88d0-489d-923f-0e611eb7199d"
    ],
    outKey: -1,
    id: 1
  };

  sliders_ws = new WebSocket(
    "wss://" +
      config[env].host +
      config[env].prefix +
      "app/" +
      appid +
      "?reloadUri=https://" +
      config[env].host +
      config[env].prefix +
      "dev-hub/engine-api-explorer&v=" +
      new Date()
  );

  sliders_ws.onopen = function(msg) {
    // Logic for opened connection
    console.log("Slider connection opened");
    sliders_ws.send(JSON.stringify(OpenDoc));
  };

  sliders_ws.onerror = function() {
    console.log("Error!!", "Some issues fetching Slider");
  };
}

function LoadSliderData(objId, callback) {
  GetDateObject.params.qId = objId;

  if (!sliders_ws) {
    SliderOpenWS();
  }

  sendMessage(sliders_ws, JSON.stringify(GetDateObject));

  sliders_ws.onmessage = function(data, flags) {
    var wsres = JSON.parse(data.data);

    if (wsres.id == 2) {
      sliders_ws.send(JSON.stringify(GetLayoutData));
    }

    if (wsres.id == 3) {
      if (callback) {
        callback(wsres);
      }
    }
  };
}
