var data = {
  // appid: {
  //   appid: "f423bd3c-587b-40b0-8668-b2e87c56996b",
  //   appiid: "8e3cb028-88d0-489d-923f-0e611eb7199d"
  // },

  devlocal: {
    appid: "6e01a888-55e5-4d72-9562-49e6d0e0781f",
    //appid: "b6632fa1-bfba-43de-92b0-aa3a9d043c3f",
    virproxy: "virproxy"
  },

  dev: {
    appid: "d1bfc8d2-ffc9-4c04-9594-f238f92d643a",
    virproxy: "axis-dev"
  },
  test: {
    appid: "0fd1f6f6-c1ac-4025-9551-e198b4a9c816",
    virproxy: "axis-test"
  },
  prod: {
    appid: "92b5a859-0fc0-4a79-8b4c-e8c9e157e341",
    virproxy: "axis"
  },

  pipeline: {
    pipe_qv01: { objId: "c5fb6410-87aa-40c2-ba0a-4f0832b099f5" },
    pipe_qv02: { objId: "Hnknjek" },
    pipe_qv03: { objId: "jGpxPN" },
    "filter-kfc-pipeline1": {
      objId: "mSXFeQJ",
      fieldName: "Asset Country",
      filterName: "Country"
    },
    "filter-kfc-pipeline2": {
      objId: "FabjZ",
      fieldName: "Asset Status",
      filterName: "Status"
    },
    "filter-kfc-pipeline3": {
      objId: "JrWjF",
      fieldName: "Asset Name",
      filterName: "Asset"
    },
    "filter-kfc-pipeline4": {
      objId: "CButyvP",
      fieldName: "Asset Seller Agent",
      filterName: "Seller Agent"
    },
    "filter-kfc-pipeline5": {
      objId: "BZyGr",
      fieldName: "Asset Seller",
      filterName: "Seller"
    },
    "filter-kfc-pipeline6": {
      objId: "rABKFhb",
      fieldName: "Asset City",
      filterName: "City"
    },

    pipe_qv03: { divId: "pipe_qv03", objId: "jGpxPN" },
    pipe_qv04: { divId: "pipe_qv04", objId: "uPsdPJ" },
    pipe_qv04_1: { divId: "pipe_qv04_1", objId: "HTdXRT" },
    pipe_qv05: { divId: "pipe_qv05", objId: "WkYQkvV" },
    pipe_qv06: { divId: "pipe_qv06", objId: "aRRthq" },
    pipe_qv07: { divId: "pipe_qv07", objId: "qMgzpYr" },
    pipe_qv08: { divId: "pipe_qv08", objId: "hHvYePj" },
    pipe_qv09: { divId: "qvpipe_qv0920", objId: "ebhEx" },
    pipe_qv10: { divId: "pipe_qv10", objId: "WwSfmt" },
    pipe_qv11: { divId: "pipe_qv11", objId: "tczeJyu" },
    pipe_qv12: { divId: "pipe_qv12", objId: "DmGxT" },
   
    // "filter-kfc-pipeline6": { objId: "vHRBYSJ", fieldName: "Asset Lot Size",filterName:"Lot Size" },
    // "filter-kfc-pipeline7": { objId: "PrFsmnS", fieldName: "Asset Lot Size",filterName:"NIY" },
    // "filter-kfc-pipeline8": { objId: "jRjdyHx", fieldName: "Rent GLA (in SQ M)",filterName:"GLA" },
  },
  totalsalesovertime: {
    "filter-kfc-totalsales1": {
      objId: "NzHtJSR",
      fieldName: "Asset Name",
      filterName: "Asset"
    },
    "filter-kfc-totalsales2": {
      objId: "qHKzCdr",
      fieldName: "Year",
      filterName: "Year"
    },
    "filter-kfc-totalsales3": {
      objId: "SxQVkCm",
      fieldName: "Quarter",
      filterName: "Quarter"
    },
    "filter-kfc-totalsales4": {
      objId: "npvnZ",
      fieldName: "Month",
      filterName: "Month"
    }
  },
  likeforlike: {
    "filter-kfc-likeforlike1": {
      objId: "DfSp",
      fieldName: "Asset Status",
      filterName: "Status"
    },
    "filter-kfc-likeforlike2": {
      objId: "uaxFJt",
      fieldName: "Brand Name",
      filterName: "Brand"
    },
    "filter-kfc-likeforlike3": {
      objId: "dRskkJ",
      fieldName: "Retailer Category",
      filterName: "Retailer Category"
    },
    "filter-kfc-likeforlike4": {
      objId: "WffVmKm",
      fieldName: "Unit Size Text",
      filterName: "Unit Size"
    },
    "filter-kfc-likeforlike5": {
      objId: "ecmcdj",
      fieldName: "Level",
      filterName: "Floor"
    },
    "filter-kfc-likeforlike6": {
      objId: "YPzj",
      fieldName: "Asset Country",
      filterName: "Country"
    },
    "filter-kfc-likeforlike7": {
      objId: "budDPr",
      fieldName: "Asset Name",
      filterName: "Asset"
    },
    "filter-kfc-likeforlike8": {
      objId: "uJeCw",
      fieldName: "Anchor Flag",
      filterName: "Anchor"
    },
    "filter-kfc-likeforlike9": {
      objId: "pzgvEPm",
      fieldName: "Asset Retail Class",
      filterName: "Retail Asset Class"
    },
    "filter-kfc-likeforlike10": {
      objId: "NjCNuE",
      fieldName: "Unit Type",
      filterName: "Unit Type"
    },
    LikeForLike_qv01: { divId: "LikeForLike_qv01", objId: "kGmNb" },
    LikeForLike_qv02: { divId: "LikeForLike_qv02", objId: "ngGWj" },
    LikeForLike_qv05: { divId: "LikeForLike_qv05", objId: "fUBYbZ" },
    LikeForLike_qv06: { divId: "LikeForLike_qv06", objId: "mmAUyk" },
    LikeForLike_qv07: { divId: "LikeForLike_qv07", objId: "RPYZzYN" },
    LikeForLike_qv08: { divId: "LikeForLike_qv08", objId: "jtspDj" },
    LikeForLike_qv08ON: { divId: "LikeForLike_qv08ON", objId: "ZTCbNu" },
    LikeForLike_qv09: { divId: "LikeForLike_qv09", objId: "WTDZuA" },
    LikeForLike_qv10: { divId: "LikeForLike_qv10", objId: "smzUBx" },



  },
  salesdensitytrends: {
    "filter-kfc-salesdensity1": {
      objId: "sxpnqH",
      fieldName: "Level",
      filterName: "Floor"
    },
    "filter-kfc-salesdensity2": {
      objId: "rUTpbT",
      fieldName: "Brand Name",
      filterName: "Brand"
    },
    "filter-kfc-salesdensity3": {
      objId: "CmeNFE",
      fieldName: "Retailer Category",
      filterName: "Retailer Category"
    },
    "filter-kfc-salesdensity4": {
      objId: "pbtPxe",
      fieldName: "Unit Size Text",
      filterName: "Unit Size"
    },
    "filter-kfc-salesdensity5": {
      objId: "DfSp",
      fieldName: "Asset Status",
      filterName: "Status"
    },
    "filter-kfc-salesdensity6": {
      objId: "NtsJpPK",
      fieldName: "Asset Country",
      filterName: "Country"
    },
    "filter-kfc-salesdensity7": {
      objId: "GJZLbP",
      fieldName: "Asset Name",
      filterName: "Asset"
    },
    "filter-kfc-salesdensity8": {
      objId: "sDbxZDK",
      fieldName: "Anchor Flag",
      filterName: "Anchor"
    },
    "filter-kfc-salesdensity9": {
      objId: "WUTD",
      fieldName: "Asset Retail Class",
      filterName: "Retail Asset Class"
    },
    "filter-kfc-salesdensity10": {
      objId: "jTAHhmR",
      fieldName: "Unit Type",
      filterName: "Unit Type"
    },

    salestrends_qv01: { divId: "salestrends_qv01", objId: "TSvLXGt" },
    salestrends_qv02: { divId: "salestrends_qv02", objId: "CxHnvN" },
    salestrends_qv03: { divId: "salestrends_qv03", objId: "91881ddd-9e09-4518-9b72-7817f0cf06d4" },
    salestrends_qv04: { divId: "salestrends_qv04", objId: "aAzRUA" },

    salestrends_qv011: { divId: "salestrends_qv011", objId: "cbae3c1a-339e-465c-b5d5-e16aff88879b" },
    salestrends_qv012: { divId: "salestrends_qv012", objId: "93a5cdab-b08a-46e7-9fbf-55662df0d076" },
    salestrends_qv013: { divId: "salestrends_qv013", objId: "5c9124ed-3ff7-4679-ade6-9f604b106f25" },
    salestrends_qv014: { divId: "salestrends_qv014", objId: "4d70a442-40ed-4d4d-a927-b5cfe1935592" },
    salestrends_qv015: { divId: "salestrends_qv015", objId: "46e2fd30-5a18-4997-9d24-9b74caad7c9d" },
    salestrends_qv016: { divId: "salestrends_qv016", objId: "55b84c3d-2267-44f0-a8d4-be53ee45bed1" },
    salestrends_qv017: { divId: "salestrends_qv017", objId: "93f9739e-caec-4284-90a0-475dd619cb51" },
    salestrends_qv018: { divId: "salestrends_qv018", objId: "3a55ff60-6d3c-4829-a12d-714f1fc89a72" },
    salestrends_qv019: { divId: "salestrends_qv019", objId: "a5d7e1f5-be11-49e4-a2b9-0aecb139070a" },
  
  },
  nonsalesreporting: {
    "filter-kfc-nonsales1": {
      objId: "TXPpGst",
      fieldName: "Retailer Category",
      filterName: "Retailer Category"
    },
    "filter-kfc-nonsales2": {
      objId: "LArwTu",
      fieldName: "Level",
      filterName: "Floor"
    },
    "filter-kfc-nonsales3": {
      objId: "nPARXW",
      fieldName: "Brand Name",
      filterName: "Brand"
    },
    "filter-kfc-nonsales4": {
      objId: "bwuPTD",
      fieldName: "Unit Size Text",
      filterName: "Unit Size"
    },
    // "filter-kfc-nonsales5": {
    //   objId: "bBtBHxP",
    //   fieldName: "Rent Next Break Date Text",
    //   filterName: "Next Break Date"
    // },
    // "filter-kfc-nonsales6": {
    //   objId: "qySWmMZ",
    //   fieldName: "Lease Start Date Text",
    //   filterName: "Lease Start Date"
    // },
    // "filter-kfc-nonsales7": {
    //   objId: "egapqdq",
    //   fieldName: "Lease End Date Text",
    //   filterName: "Lease End Date"
    // },
    "filter-kfc-nonsales8": {
      objId: "DfSp",
      fieldName: "Asset Status",
      filterName: "Status"
    },
    "filter-kfc-nonsales9": {
      objId: "Mgvh",
      fieldName: "Asset Country",
      filterName: "Country"
    },
    "filter-kfc-nonsales10": {
      objId: "wETvzR",
      fieldName: "Asset Name",
      filterName: "Asset"
    },
    "filter-kfc-nonsales11": {
      objId: "YsLHuH",
      fieldName: "Anchor Flag",
      filterName: "Anchor"
    },
    "filter-kfc-nonsales12": {
      objId: "JPMUvz",
      fieldName: "Asset Retail Class",
      filterName: "Retail Asset Class"
    },
    "filter-kfc-nonsales13": {
      objId: "WwQBj",
      fieldName: "Unit Type",
      filterName: "Unit Type"
    },
    "filter-kfc-nonsales14": {
      objId: "zfePz",
      fieldName: "Letting Status",
      filterName: "Letting Status"
    },
    nonsalesreporting_qv01: { divId: "nonsalesreporting_qv01", objId: "PgeyEa" },
    nonsalesreporting_qv02: { divId: "nonsalesreporting_qv02", objId: "fUBYbZ" },
    nonsalesreporting_qv03: { divId: "nonsalesreporting_qv03", objId: "mmAUyk" },
  
  },
  footfalls: {
    "filter-kfc-footfalls1": {
      objId: "DfSp",
      fieldName: "Asset Status",
      filterName: "Status"
    },
    footfall_qv01: { divId: "footfall_qv01", objId: "WkVXz" },
    footfall_qv02: { divId: "footfall_qv02", objId: "hKJcq" },
    footfall_qv03: { divId: "footfall_qv03", objId: "fUBYbZ" },
    footfall_qv04: { divId: "footfall_qv04", objId: "mmAUyk" },
    footfall_qv05: { divId: "footfall_qv05", objId: "GpPVn" },
    footfall_qv06: { divId: "footfall_qv06", objId: "XsRgjB" },
    footfall_qv07: { divId: "footfall_qv07", objId: "CXpbJw" },
    footfall_qv08: { divId: "footfall_qv08", objId: "PBXpP" },
    footfall_qv09: { divId: "footfall_qv09", objId: "xXXDBXH" },
    footfall_qv10: { divId: "footfall_qv10", objId: "NgeRp" },
   
  },
  tenantdashboard: {
    "filter-kfc-tenantdash_01": {
      objId: "DfSp",
      fieldName: "Asset Status",
      filterName: "Status"
    },
    tenantdash_qv01: { divId: "tenantdash_qv01", objId: "jpGx" },
    tenantdash_qv02: { divId: "tenantdash_qv02", objId: "bYhEPj" },
    tenantdash_qv03: { divId: "tenantdash_qv03", objId: "mXmMZv" },
    tenantdash_qv04: { divId: "tenantdash_qv04", objId: "HmPDEuc" },
    tenantdash_qv05: { divId: "tenantdash_qv05", objId: "pYeEnmW" },
    tenantdash_qv06: { divId: "tenantdash_qv06", objId: "SNWJStH" },
    tenantdash_qv07: { divId: "tenantdash_qv07", objId: "GAbj" },
    tenantdash_qv08: { divId: "tenantdash_qv08", objId: "dgppmS" },
    tenantdash_qv09: { divId: "tenantdash_qv09", objId: "xvwsg" },
    tenantdash_qv10: { divId: "tenantdash_qv10", objId: "psDZHn" },
    tenantdash_qv11: { divId: "tenantdash_qv11", objId: "NggTaNz" },
    tenantdash_qv12: { divId: "tenantdash_qv12", objId: "zqMcSF" },
    tenantdash_qv13: { divId: "tenantdash_qv13", objId: "WLMnqhv" },
    tenantdash_qv14: { divId: "tenantdash_qv14", objId: "JmvMj" },
    tenantdash_qv15: { divId: "tenantdash_qv15", objId: "AZYgmSD" },
  
  },
  tenancySchedule: {
    "filter-kfc-tenancy_01": {
      objId: "DfSp",
      fieldName: "Asset Status",
      filterName: "Status"
    },
    tenancy_qv01: { divId: "tenancy_qv01", objId: "WRWjJ" },
    tenancy_qv02: { divId: "tenancy_qv02", objId: "dquCFmg" },
    tenancy_qv03: { divId: "tenancy_qv03", objId: "vSdY" },
    tenancy_qv04: { divId: "tenancy_qv04", objId: "ZEPsYpx" },
    tenancy_qv05: { divId: "tenancy_qv05", objId: "BaXFXk" },
    tenancy_qv06: { divId: "tenancy_qv06", objId: "RJL" },
    tenancy_qv07: { divId: "tenancy_qv07", objId: "WhmsDUr" },
    tenancy_qv08: { divId: "tenancy_qv08", objId: "TQqBs" },
    tenancy_qv09: { divId: "tenancy_qv09", objId: "kRuTL" },
    tenancy_qv10: { divId: "tenancy_qv10", objId: "vJaVFP" },
    tenancy_qv11: { divId: "tenancy_qv11", objId: "Fydun" },
    tenancy_qv12: { divId: "tenancy_qv12", objId: "jmeSQr" },
   
  },
  ocr: {
    "filter-kfc-ocr1": {
      objId: "RgMBPC",
      fieldName: "Retailer Category",
      filterName: "Retailer Category"
    },
    "filter-kfc-ocr2": {
      objId: "jJHmm",
      fieldName: "Unit Size Text",
      filterName: "Unit Size"
    },
    "filter-kfc-ocr3": {
      objId: "FCYPxaR",
      fieldName: "Level",
      filterName: "Floor"
    },
    "filter-kfc-ocr4": {
      objId: "EvJRqE",
      fieldName: "Brand Name",
      filterName: "Brand"
    },
    "filter-kfc-ocr5": {
      objId: "DfSp",
      fieldName: "Asset Status",
      filterName: "Status"
    },
    "filter-kfc-ocr6": {
      objId: "Mussfz",
      fieldName: "Asset Country",
      filterName: "Country"
    },
    "filter-kfc-ocr7": {
      objId: "KTNHS",
      fieldName: "Asset Name",
      filterName: "Asset"
    },
    "filter-kfc-ocr8": {
      objId: "sHepG",
      fieldName: "Anchor Flag",
      filterName: "Anchor"
    },
    "filter-kfc-ocr9": {
      objId: "pvvb",
      fieldName: "Asset Retail Class",
      filterName: "Retail Asset Class"
    },
    "filter-kfc-ocr10": {
      objId: "BnKpjU",
      fieldName: "Unit Type",
      filterName: "Unit Type"
    },
    ocr_qv01: { divId: "ocr_qv01", objId: "bRQdgqg" },
    ocr_qv03: { divId: "ocr_qv03", objId: "fUBYbZ" },
    ocr_qv04: { divId: "ocr_qv04", objId: "mmAUyk" },

  },
  expiryprofile: {
    "filter-kfc-expiry1": {
      objId: "BaxPJ",
      fieldName: "Retailer Category",
      filterName: "Retailer Category"
    },
    "filter-kfc-expiry2": {
      objId: "ymYGuy",
      fieldName: "Level",
      filterName: "Floor"
    },
    "filter-kfc-expiry3": {
      objId: "LLjz",
      fieldName: "Rent Next Break Date Text",
      filterName: "Next Break Date"
    },
    "filter-kfc-expiry4": {
      objId: "RYmUZp",
      fieldName: "Rent Next Break Month Text",
      filterName: "Next Break Month"
    },
    "filter-kfc-expiry5": {
      objId: "RPpzTQ",
      fieldName: "Rent Next Break Year Text",
      filterName: "Next Break Year"
    },
    "filter-kfc-expiry6": {
      objId: "CVSJj",
      fieldName: "Lease End Date Text",
      filterName: "Lease End Date"
    },
    "filter-kfc-expiry7": {
      objId: "pDEpk",
      fieldName: "Lease End Month Text",
      filterName: "Lease End Month"
    },
    "filter-kfc-expiry8": {
      objId: "ujMAVu",
      fieldName: "Lease End Year Text",
      filterName: "Lease End Year"
    },
    "filter-kfc-expiry9": {
      objId: "ZjwHbm",
      fieldName: "Brand Name",
      filterName: "Brand"
    },
    "filter-kfc-expiry10": {
      objId: "DfSp",
      fieldName: "Asset Status",
      filterName: "Status"
    },
    "filter-kfc-expiry11": {
      objId: "BuPmJg",
      fieldName: "Asset Country",
      filterName: "Country"
    },
    "filter-kfc-expiry12": {
      objId: "jbhrL",
      fieldName: "Asset Name",
      filterName: "Asset"
    },
    "filter-kfc-expiry13": {
      objId: "ACbvjss",
      fieldName: "Anchor Flag",
      filterName: "Anchor"
    },
    "filter-kfc-expiry14": {
      objId: "ZZDPLF",
      fieldName: "Asset Retail Class",
      filterName: "Retail Asset Class"
    },
    "filter-kfc-expiry15": {
      objId: "WajDNR",
      fieldName: "Unit Size",
      filterName: "Unit Size"
    },
    "filter-kfc-expiry16": {
      objId: "GHHAeHz",
      fieldName: "Unit Type",
      filterName: "Unit Type"
    },
    expiry_qv01: { divId: "expiry_qv01", objId: "tPdTpy" },
    expiry_qv02: { divId: "expiry_qv02", objId: "WGSFgBj" },
    expiry_qv03: { divId: "expiry_qv03", objId: "zKnKqTf" },
    expiry_qv04: { divId: "expiry_qv04", objId: "fUBYbZ" },
    expiry_qv05: { divId: "expiry_qv05", objId: "mmAUyk" },
    expiry_qv0Date: { divId: "expiry_qv0Date", objId: "YkVThp" },
   

  },
  performancereport: {
    "filter-kfc-performance1": {
      objId: "dRQzefx",
      fieldName: "Brand Name",
      filterName: "Brand"
    },
    "filter-kfc-performance2": {
      objId: "mnjhNh",
      fieldName: "Retailer Category",
      filterName: "RETAILER CATEGORY"
    },
    "filter-kfc-performance3": {
      objId: "gqzCPm",
      fieldName: "Unit Size Text",
      filterName: "Unit Size"
    },
    "filter-kfc-performance4": {
      objId: "PWFURfL",
      fieldName: "Level",
      filterName: "Floor"
    },
    // "filter-kfc-performance5": {
    //   objId: "mCuvWG",
    //   fieldName: "Lease Start Date Text",
    //   filterName: "Lease Start Date"
    // },
    // "filter-kfc-performance6": {
    //   objId: "GpYQJPM",
    //   fieldName: "Lease End Date Text",
    //   filterName: "Lease End Date"
    // },
    // "filter-kfc-performance7": {
    //   objId: "sFDfpq",
    //   fieldName: "Rent Next Break Date Text",
    //   filterName: "Next Break Date"
    // },
    "filter-kfc-performance8": {
      objId: "dRJkNK",
      fieldName: "Anchor Flag",
      filterName: "Anchor"
    },
    "filter-kfc-performance9": {
      objId: "DfSp",
      fieldName: "Asset Status",
      filterName: "Status"
    },
    "filter-kfc-performance10": {
      objId: "mFRGP",
      fieldName: "Asset Country",
      filterName: "Country"
    },
    "filter-kfc-performance11": {
      objId: "bQnxCM",
      fieldName: "Asset Name",
      filterName: "Asset"
    },
    "filter-kfc-performance12": {
      objId: "MkaFJrL",
      fieldName: "Asset Retail Class",
      filterName: "Retail Asset Class"
    },
    "filter-kfc-performance13": {
      objId: "tpAjR",
      fieldName: "Unit Type",
      filterName: "Unit Type"
    },
    "filter-kfc-performance14": {
      objId: "BUtkG",
      fieldName: "Rent Next Break Year Text",
      filterName: "Next Break Year"
    },
    "filter-kfc-performance15": {
      objId: "egHqTp",
      fieldName: "Lease End Year Text",
      filterName: "Lease End Year"
    },

    performance_qv01: { divId: "performance_qv01", objId: "LWmppk" },
    performance_qv02: { divId: "performance_qv02", objId: "fUBYbZ" },
    performance_qv03: { divId: "performance_qv03", objId: "mmAUyk" },
    performance_qv03ON: { divId: "performance_qv03ON", objId: " nANaRj" },
    performance_qv04: { divId: "performance_qv04", objId: "jLYaKA" },
  
  },
  overview: {
    "filter-kfc-overview1": {
      objId: "gxwjsr",
      fieldName: "Brand Name",
      filterName: "Brand"
    },
    "filter-kfc-overview2": {
      objId: "dNAuJHX",
      fieldName: "Level",
      filterName: "Floor"
    },
    "filter-kfc-overview3": {
      objId: "Fsejr",
      fieldName: "Retailer Category",
      filterName: "Retailer Category"
    },
    "filter-kfc-overview4": {
      objId: "GeJJu",
      fieldName: "Unit Type",
      filterName: "Unit Type"
    },
    "filter-kfc-overview5": {
      objId: "DfSp",
      fieldName: "Asset Status",
      filterName: "Status"
    },
    "filter-kfc-overview6": {
      objId: "ncUSFmq",
      fieldName: "Asset Country",
      filterName: "Country"
    },
    "filter-kfc-overview7": {
      objId: "XaJFh",
      fieldName: "Asset Name",
      filterName: "Asset"
    },
    "filter-kfc-overview8": {
      objId: "YmQCPE",
      fieldName: "Anchor Flag",
      filterName: "Anchor"
    },
    "filter-kfc-overview9": {
      objId: "STbMvJ",
      fieldName: "Asset Retail Class",
      filterName: "Retail Asset Class"
    },
    "filter-kfc-overview10": {
      objId: "QcNyfJ",
      fieldName: "Unit Size Text",
      filterName: "Unit Size"
    },

    overview_qv01: { divId: "overview_qv01", objId: "umxWmWb" },
    facebookads_kpi1: { divId: "facebookads_kpi1", objId: "dvVBeuR" },
    facebookads_kpi6: { divId: "facebookads_kpi6", objId: "pfKD" },
    overview_qv02: { divId: "overview_qv02", objId: "Kvnn" },
    overview_qv03: { divId: "overview_qv03", objId: "xnQWhN" },
    overview_qv04: { divId: "overview_qv04", objId: "JbRmm" },
    overview_qv05: { divId: "overview_qv05", objId: "vgtUaQ" },
    overview_qv06: { divId: "overview_qv06", objId: "kDcwm" },
    overview_qv07: { divId: "overview_qv07", objId: "BWdmNFt" },
    overview_qv08: { divId: "overview_qv08", objId: "EFgVv" },
    overview_qv09: { divId: "overview_qv09", objId: "DKwee" },
    overview_qv10: { divId: "overview_qv10", objId: "JcLSgh" },
    overview_qv11: { divId: "overview_qv11", objId: "pJCCMz" },
    overview_qv12: { divId: "overview_qv12", objId: "XrsrYh" },
    overview_qv13: { divId: "overview_qv13", objId: "LLJhwnN" },
    overview_qv13_1: { divId: "overview_qv13_1", objId: "VCJu" },
    overview_qv21: { divId: "overview_qv21", objId: "xAeQXut" },
    overview_qv14: { divId: "overview_qv14", objId: "kEMCdJ" },
    overview_qv24: { divId: "overview_qv24", objId: "YwLNab" },
    overview_qv25: { divId: "overview_qv25", objId: "pvjWmU" },
    overview_qv15: { divId: "overview_qv15", objId: "XXJJm" },
    overview_qv16: { divId: "overview_qv16", objId: "CTFLf" },
    overview_qv17: { divId: "overview_qv17", objId: "EPajFv" },
    overview_qv18: { divId: "overview_qv18", objId: "NsTHxQE" },
    overview_qv19: { divId: "overview_qv19", objId: "WxpdP" },
    overview_qv20: { divId: "overview_qv20", objId: "ahjJjmZ" }, 
    overview_qv26: { divId: "overview_qv26", objId: "fUBYbZ" },
    overview_qv29: { divId: "overview_qv29", objId: "SQWLR" },
    overview_qv30: { divId: "overview_qv30", objId: "VPpuZTK" },
    overview_qv31: { divId: "overview_qv31", objId: "LLJhwnN" },
    // overview_qv32: { divId: "overview_qv32", objId: "JvumUkG" },
    overview_qv33: { divId: "overview_qv33", objId: "mmAUyk" },
    overview_qv34: { divId: "overview_qv34", objId: "PPs" },
    overview_qv35: { divId: "overview_qv35", objId: "jtrKSX" },
    overview_qv36: { divId: "overview_qv36", objId: "qfrLFnD" },
    overview_qv39: { divId: "overview_qv39", objId: "tPdTpy" },
    overview_qv38: { divId: "overview_qv38", objId: "zKnKqTf" },
    overview_qvDate38: { divId: "overview_qvDate38", objId: "YkVThp" },
    overview_qv40: { divId: "overview_qv40", objId: "pbcEKc" },
    overview_qv41: { divId: "overview_qv41", objId: "Spzfwt" },
    overview_qv42: { divId: "overview_qv42", objId: "hjkyVG" },
    overview_qv43: { divId: "overview_qv43", objId: "DAWyM" },
    overview_qv44: { divId: "overview_qv44", objId: "drZahmD" },
    overview_qv45: { divId: "overview_qv45", objId: "eJPmj" },
    overview_qv46: { divId: "overview_qv46", objId: "PZBcmE" },
    overview_qv44Toggle: { divId: "overview_qv44Toggle", objId: "tFEcke" },
    overview_qv47: { divId: "overview_qv47", objId: "aZLusAS" },
    overview_qv48: { divId: "overview_qv48", objId: "ptSjLm" },
    overview_qv49: { divId: "overview_qv49", objId: "fHgqhn" },
    overview_qv50: { divId: "overview_qv50", objId: "GpCUgYk" },
    overview_qv51: { divId: "overview_qv51", objId: "eMcbZb" },
    overview_qv52: { divId: "overview_qv52", objId: "mBpFNj" },
    overview_qv53: { divId: "overview_qv53", objId: "hjrdU" },
    overview_qv54: { divId: "overview_qv54", objId: "gzesj" },
    overview_qv55: { divId: "overview_qv55", objId: "LWbkhV" },
    overview_qv56: { divId: "overview_qv56", objId: "jCzZR" },
    overview_qv54_1: { divId: "overview_qv54_1", objId: "zQXafXf" },
    overview_qv54_2: { divId: "overview_qv54_2", objId: "tEyXxq" },
    overview_qv54_3: { divId: "overview_qv54_3", objId: "BmhZL" },
    overview_qv54_4: { divId: "overview_qv54_4", objId: "PUtJsw" },
  
  },
  tenantBenchmarking: {
    
    tenantbench_qv01: { divId: "tenantbench_qv01", objId: "NBhmfdm" },
    tenantbench_qv02: { divId: "tenantbench_qv02", objId: "PdCarw" },
    tenantbench_qv03: { divId: "tenantbench_qv03", objId: "jCaGkn" },
    tenantbench_qv04: { divId: "tenantbench_qv04", objId: "jCaGkn" },
    tenantbench_qv05: { divId: "tenantbench_qv05", objId: "vFApXJS" },
    tenantbench_qv06: { divId: "tenantbench_qv06", objId: "dpDJZH" },
    tenantbench_qv07: { divId: "tenantbench_qv07", objId: "DSDyy" },
    tenantbench_qv08: { divId: "tenantbench_qv08", objId: "cTQfxT" },
    tenantbench_qv09: { divId: "tenantbench_qv09", objId: "QpEC" },
    tenantbench_qv10: { divId: "tenantbench_qv10", objId: "mzcS" },
    tenantbench_qv11: { divId: "tenantbench_qv11", objId: "BfyzsYH" },
    tenantbench_qv12: { divId: "tenantbench_qv12", objId: "phamuE" },
   
  },
  salesforceslicer:{
    kpi01: { divId: "kpi01", objId: "ed2b40e3-1e11-4754-9431-2bba0a1b7b3a" },
    kpi02: { divId: "kpi02", objId: "5001b050-82f3-4a85-84b0-1e3bcacd5198" },
    kpi03: { divId: "kpi03", objId: "d2f0df89-25b5-463e-b69d-c04b7b814308" },
    kpi04: { divId: "kpi04", objId: "7e6bdba1-88a6-406c-b281-7fa6ebe092fa" },
    kpi05: { divId: "kpi05", objId: "eb076dbb-2465-454e-b7ed-b9bcca6cb825" },
   // kpi06: { divId: "kpi06", objId: "f968729a-3419-4940-9c34-c80c4018be7a" },
    kpi07: { divId: "kpi07", objId: "b2798bb1-b120-4400-87e5-1e00adbe9361" },
    kpi08: { divId: "kpi08", objId: "7c7a4b93-a2ad-461e-933b-d5d4d937443c" },
    kpi09: { divId: "kpi09", objId: "f22baa0d-9e6d-4c63-8452-5e362dce4025" },
    kpi10: { divId: "kpi10", objId: "a6ccc84b-cd42-46fd-8361-830e55dadcfc" },
    
    // kpi11: { divId: "kpi11", objId: "ce03c170-34af-446e-8d27-b16aa6909d59" },
    // kpi12: { divId: "kpi12", objId: "48540af2-b136-45e4-b388-f60a326caba5" },
    // kpi13: { divId: "kpi13", objId: "849df6a5-6977-49b6-9e44-de16ca3ec045" },
    // kpi14: { divId: "kpi14", objId: "d53d3dbe-b84b-4bb4-a114-1c1fff93434f" },
    // kpi15: { divId: "kpi15", objId: "919f6287-f7ac-405c-bfa5-4faf82235eee" },
    // kpi16: { divId: "kpi16", objId: "8d61f35d-f546-405f-96bc-a78dedcb14b2" },
    // kpi17: { divId: "kpi17", objId: "586df167-8fd1-4aed-a08f-a6ba0b61d42b" },
    // kpi18: { divId: "kpi18", objId: "d0c5b503-c0ec-474c-9e39-760749805e0c" },
    // kpi19: { divId: "kpi19", objId: "a9d6dd39-3556-4da0-af52-086d454079b5" },
    
    SalesForceFilter: { divId: "SalesForceFilter", objId: "63ec8b8a-24a8-4377-877f-1fa0bd6723c4" },
  }
};


