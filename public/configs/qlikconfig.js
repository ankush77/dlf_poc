var config = {
  devlocal: {
   // appid: "b6632fa1-bfba-43de-92b0-aa3a9d043c3f",
    appid: "6e01a888-55e5-4d72-9562-49e6d0e0781f",
    baseUrl: "https://qlikdemo.polestarllp.com/virproxy/resources",
    prefix: "/virproxy/",
    port: 443,
    host: "qlikdemo.polestarllp.com",
    isSecure: true,
    config: {
      text: {
        useXhr: function(url, protocol, hostname, port) {
          // enable xhr load for all text resources
          return true;
        },
        createXhr: function() {
          // use withCredentials flag to prevent authentication errors
          var xhr = new XMLHttpRequest();
          xhr.withCredentials = true;
          return xhr;
        }
      }
    }
  },
  dev: {
    appid: "d1bfc8d2-ffc9-4c04-9594-f238f92d643a",
    baseUrl: "https://dev.instantview.io/inview-dev/resources",
    prefix: "/inview-dev/",
    port: 443,
    host: "dev.instantview.io",
    isSecure: true,
    config: {
      text: {
        useXhr: function(url, protocol, hostname, port) {
          // enable xhr load for all text resources
          return true;
        },
        createXhr: function() {
          // use withCredentials flag to prevent authentication errors
          var xhr = new XMLHttpRequest();
          xhr.withCredentials = true;
          return xhr;
        }
      }
    }
  },
  test: {
    appid: "0fd1f6f6-c1ac-4025-9551-e198b4a9c816",
    baseUrl: "https://dev.instantview.io/inview-test/resources",
    prefix: "/inview-test/",
    port: 443,
    host: "dev.instantview.io",
    isSecure: true,
    config: {
      text: {
        useXhr: function(url, protocol, hostname, port) {
          // enable xhr load for all text resources
          return true;
        },
        createXhr: function() {
          // use withCredentials flag to prevent authentication errors
          var xhr = new XMLHttpRequest();
          xhr.withCredentials = true;
          return xhr;
        }
      }
    }
  },
  prod: {
    appid: "92b5a859-0fc0-4a79-8b4c-e8c9e157e341",
    baseUrl: "https://qsprod.in-view.io:443/inview/resources",
    prefix: "/inview/",
    port: 443,
    host: "qsprod.in-view.io",
    config: {
      text: {
        useXhr: function(url, protocol, hostname, port) {
          // enable xhr load for all text resources
          return true;
        },
        createXhr: function() {
          // use withCredentials flag to prevent authentication errors
          var xhr = new XMLHttpRequest();
          xhr.withCredentials = true;
          return xhr;
        }
      }
    }
  }
};

var qlikapp = "";
