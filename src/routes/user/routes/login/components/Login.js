import React from "react";
import APPCONFIG from "constants/appConfig";
import { userActions } from "../../../../../actions/userActions";
import { connect } from "react-redux";
import logo from "assets/images/AXIS_LOGO.png";
import LoadingComponent from "components/Loading/Loader";
import DLFLogo from "../../../../../../src/assets/images/dlf60.png"
import "./login.css";

class Login extends React.Component {
  constructor() {
    super();
    this.state = {
      brand: APPCONFIG.brand,
      username: "",
      password: "",
      errors: {},
      loading: false
    };
    localStorage.setItem("loginFailure", "");
    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }
  onSubmit(e) {
    e.preventDefault();
    const LoginRequest = {
      username: this.state.username,
      password: this.state.password
    };

    if (LoginRequest.username === "") {
      this.setState({
        errors: {
          username: "Please Enter the username"
        }
      });
    } else if (LoginRequest.password === "") {
      this.setState({
        errors: {
          password: "Please Enter the password"
        }
      });
    } else {
      // this.setState({ loading: true });
      // localStorage.setItem("loginFailure", "");
      // this.props.dispatch(userAction.login(LoginRequest));
      
    var flag = false;
    for (var i = 0; i < LoginRequest.username.length; i++) {
      var strChar = LoginRequest.username.charAt(i);
      if (!/([A-Za-z0-9!@#$%^&*()_.+])/.test(strChar)) {
        localStorage.setItem("loginFailure", "WRONG FORMAT");
        break;
      }
    }

    if (!flag) {
      this.setState({ loading: true });
      localStorage.setItem("loginFailure", "");
      this.props.dispatch(userActions.login(LoginRequest));
    }
    }
  }

  onChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }
  componentDidMount() {
    if (this.props.security.validToken) {
      this.props.history.push("/app/dashboard");
    }
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.security.validToken) {
      this.props.history.push("/app/dashboard");
    }
    if (nextProps.errors) {
      this.setState({ errors: nextProps.errors });
    }
  }

  render() {
    const { errors } = this.state;

    let loading = "";

    if (localStorage.getItem("loginFailure") && this.state.loading) {
      this.setState({ loading: false });
    }

    if (this.state.loading) {
      loading = (
        <div className="Loadable">
          <LoadingComponent />
        </div>
      );
    }

    return (
      // <center>
      //   {loading}
      //   <div className="fontclass loginContainer">
      //     <div className="top-header" />
      //     <div className="background-img">
      //       <div className="headline">AXIS WEB PORTAL</div>
      //     </div>
      //     <div className="login-page">
      //       <div className="form">
      //         <form className="login-form" onSubmit={this.handleSubmit}>
      //           <img src={logo} className="logo" />
      //           <p className="message">Already registered?</p>

      //           <form
      //             action="/examples/actions/confirmation.php"
      //             method="post"
      //             onSubmit={this.onSubmit}
      //           >
      //             <div className="form-group">
      //               {/* <i class="fa fa-user"></i> */}
      //               <input
      //                 error={errors.username}
      //                 label="Username"
      //                 name="username"
      //                 value={this.state.value}
      //                 onChange={this.onChange}
      //                 className="border-focus form-control form-height"
      //                 placeholder="Username"
      //                 required="required"
      //                 maxLength="20"
      //               />
      //               {errors.username && (
      //                 <div className="invalid-feedback">{errors.username}</div>
      //               )}
      //             </div>
      //             <div className="form-group mr-top">
      //               <input
      //                 error={errors.password}
      //                 label="Password"
      //                 type="password"
      //                 name="password"
      //                 value={this.state.password}
      //                 onChange={this.onChange}
      //                 id="pass"
      //                 className="border-focus form-control form-height mt-3"
      //                 placeholder="Password"
      //                 required="required"
      //                 maxLength="20"
      //               />
      //               {errors.password && (
      //                 <div className="invalid-feedback">{errors.password}</div>
      //               )}
      //             </div>

      //             <div className="card-action border-0">
      //               {localStorage.getItem("loginFailure") ? (
      //                 <div id="errormsgid" className="errormsg">
      //                   {localStorage.getItem("loginFailure")}
      //                 </div>
      //               ) : (
      //                   ""
      //                 )}
      //               <input
      //                 type="submit"
      //                 className="color-primary login-btn"
      //                 value="Login"
      //               />
      //             </div>
      //           </form>

               
      //         </form>
      //       </div>
      //     </div>
            
      //   </div>                                          
      // </center>


 <div className="row background">
      <div className="col-md-12 dlfLogoDiv">
        <div className="col-md-4 col-sm-12 pl100">
            <img src={DLFLogo} className="dlfLogo"></img>
        </div>
      </div>
       <div className="col-md-12 center">
        <div className="form">
            <div className="loginLogo">    
            </div>
            <div className="form-group">
                <label className="textCustom" for="exampleInputEmail1">Email address</label>
                <input value={this.state.value} onChange={this.onChange} type="email" name="username" className="form-control" id="exampleInputEmail1" placeholder="Enter email" />
                {errors.username && <div className="invalid">Invalid Username</div>}
            </div>
            <div className="form-group">
                <label className="textCustom" for="exampleInputPassword1">Password</label>
                <input value={this.state.password} onChange={this.onChange} type="password" name="password" className="form-control" id="exampleInputPassword1" placeholder="Password" />
               {errors.password && <div className="invalid">Invalid Password</div>} 
            </div>
            <div className="forgetPassword textCustom">
              <a href="">Forget Password</a> 
            </div>
            <div className="textAlign">
            
            {/* <input type="submit" className="btn btn-primary" value="submit"></input> */}
            <button type="submit" onClick={this.onSubmit} className="btn btn-primary">Login</button>
            </div>
            
        </div>
    </div>
</div>
    );
  }
}

// const Page = () => <Login />;
const mapStateToProps = state => {
  return {
    users: state.users,
    security: state.security,
    errors: state.errors
  };
};
export default connect(mapStateToProps)(Login);
