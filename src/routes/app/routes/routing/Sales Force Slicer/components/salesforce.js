import React from "react";
import QueueAnim from "rc-queue-anim";
import { Grid, Paper } from "@material-ui/core";
import "./styles.scss";
import $ from "jquery";
import Tooltip from "@material-ui/core/Tooltip";
import { Typography } from "@material-ui/core";
import { userActions } from "../../../../../../actions/userActions";
import { connect } from "react-redux";
const qlikConfig = require("./../../../../../../assets/configs/config.json");

class SalesDensityTrends extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    this.props.dispatch(userActions.getHeader(true));
    const options = qlikConfig;

    let count = 0;
    var checkExist = setInterval(function() {
      if (typeof window.salesforceslicer === "function" || count > 30) {
        clearInterval(checkExist);
        window.salesforceslicer();
      } else {
        setTimeout(function() {
          count++;
        }, 100);
      }
    }, 200);
  }
  render() {
    return (
      <div className="container-fluid no-breadcrumb page-dashboard">
      {/* <div className="row ">
          {/* <div className="w-10">
            <div className="filter" id="kpi11" />
          </div>
          <div className="w-20Title">
            <div className="filter" id="kpi12" />
          </div>
          <div className="w-10">
            <div className="filter" id="kpi13" />
          </div>
          <div className="w-20Title">
            <div className="filter" id="kpi14" />
          </div>
          <div className="w-40">
            <div className="filter" id="kpi15" />
          </div> 
          
          <div className="col-md-1 col-sm-12 filterDiv">
            <div className="filterName" id="SalesForceFilter" />
          </div>
          {/* <div className="col-md-2 col-sm-12">
            <div className="filter" id="kpi16" />
          </div>
          <div className="col-md-2 col-sm-12">
            <div className="filter" id="kpi17" />
          </div>
          <div className="col-md-2 col-sm-12">
            <div className="filter" id="kpi18" />
          </div> 
           <div className="col-md-12 col-sm-12">
            <div  id="kpi19" />
          </div> 
          {/* <div className="col-md-1 col-sm-12">
            <div className="filter" id="kpi110" />
          </div>
          <div className="col-md-1 col-sm-12">
            <div className="filter" id="kpi111" />
          </div>
          <div className="col-md-1 col-sm-12">
            <div className="filter" id="kpi112" />
          </div> 
        </div> */}
        <div className="row  p15">
          <div className="w-20 ">
            <div className="kpi" id="kpi01" />
          </div>
          <div className="w-20 ">
            <div className="kpi" id="kpi02" />
          </div>
          <div className="w-20 ">
            <div className="kpi" id="kpi03" />
          </div>
          <div className="w-20 ">
            <div className="kpi" id="kpi04" />
          </div>
          <div className="w-20 ">
            <div className="kpi" id="kpi05" />
          </div>
          {/* <div className="col-md-2 col-sm-12">
            <div className="kpi" id="kpi06" />
          </div> */}
        </div>

        <div className="row mtop">
          <div className="col-md-12 col-sm-12">
            <div className="salesButton col-md-4" id="kpi07" />
          </div>
          <div className="col-md-6 col-sm-12 pr7">
            <div className="salesCharts" id="kpi08" />
          </div>
          <div className="col-md-6 col-sm-12 pl7">
            <div className="salesCharts" id="kpi09" />
          </div>
        </div>
        <div className="row mtop">
          <div className="col-md-12 col-sm-12">
            <div className="salesCharts" id="kpi10" />
          </div>
  
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => ({
  showHeader: state.showHeader
});

export default connect(mapStateToProps)(SalesDensityTrends);
