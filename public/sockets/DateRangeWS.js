var date_range_ws;

var date_range_ws_pipe;

var GetDateObject = {
  handle: 1,
  method: "GetObject",
  params: {
    qId: ""
  },
  outKey: -1,
  id: 2
};

var GetDateHyperCubeData = {
  handle: 2,
  method: "GetHyperCubeData",
  params: {
    qPath: "/qHyperCubeDef",
    qPages: [{ qTop: 0, qLeft: 0, qHeight: 10, qWidth: 10 }]
  },
  id: 3
};

function DateRangeOpenWS() {
  var OpenDoc = { handle: -1, method: "GetDocList", params: [], outKey: -1, id: 1 };

  date_range_ws = new WebSocket(
    "wss://" +
      config[env].host +
      config[env].prefix +
      "app/" +
      appid +
      "?reloadUri=https://" +
      config[env].host +
      config[env].prefix +
      "dev-hub/engine-api-explorer&v=" +
      new Date()
  );

  date_range_ws.onopen = function(msg) {
    // Logic for opened connection
    console.log("Websocket Connection opened");
    date_range_ws.send(JSON.stringify(OpenDoc));
  };

  date_range_ws.onerror = function() {
    console.log("Error!!", "Some issues fetching data");
  };
}
function DateRangeOpenWS_pipe() {
  var OpenDoc_pipe = {
    method: "OpenDoc",
    handle: -1,
    params: [
      appid
      //"8e3cb028-88d0-489d-923f-0e611eb7199d"
    ],
    outKey: -1,
    id: 1
  };

  date_range_ws_pipe = new WebSocket(
    "wss://" +
      config[env].host +
      config[env].prefix +
      "app/" +
      appid +
      "?reloadUri=https://" +
      config[env].host +
      config[env].prefix +
      "dev-hub/engine-api-explorer&v=" +
      new Date()
  );

  date_range_ws_pipe.onopen = function(msg) {
    // Logic for opened connection
    console.log("Date Range connection opened");
    date_range_ws_pipe.send(JSON.stringify(OpenDoc_pipe));
  };

  date_range_ws_pipe.onerror = function() {
    console.log("Error!!", "Some issues fetching date range");
  };
}

function LoadDateRange(callback) {
 
  if (!date_range_ws) {
    DateRangeOpenWS();
  }

    date_range_ws.onmessage = function(event) {
    
     response = JSON.parse(event.data);
      
        if(response.id == 1){
        
            callback(response);
        }
  
  };
}
function LoadDateRange_pipe(objId, callback) {
  GetDateObject.params.qId = objId;

  if (!date_range_ws_pipe) {
    DateRangeOpenWS_pipe();
  }

  sendMessage(date_range_ws_pipe, JSON.stringify(GetDateObject));

  date_range_ws_pipe.onmessage = function(data, flags) {
    var wsres = JSON.parse(data.data);

    if (wsres.id == 2) {
      date_range_ws_pipe.send(JSON.stringify(GetDateHyperCubeData));
    }

    if (wsres.id == 3) {
      if (callback) {
        callback(wsres);
      }
    }
  };
}
