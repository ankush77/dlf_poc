import { combineReducers } from "redux";
import { routerReducer } from "react-router-redux";
import settings from "./settings";
import users from "./users";
import securityRoute from "./securityRoute";
import errorReducer from "./errorReducer";
import qlikScripts from "./qlikScripts";
import caspiopage from "./ui-header";

const reducers = {
  routing: routerReducer,
  settings,
  users,
  errors: errorReducer,
  security: securityRoute,
  qlikScripts,
  showHeader: caspiopage
};

export default combineReducers(reducers);
