var flip_flop_ws;
var GetflipObjectIdInitial = 3000,
  GetflipLayoutIdInitial = 4000,
  GetflipHyperCubeIdInitial = 5000;
var flipfloplayout = {};
var idInformation = {};
// var GetFlipObject = {
//   handle: 1,
//   method: "GetObject",
//   params: {
//     qId: ""
//   },
//   outKey: -1,
//   id: 100
// };

// var GetFlipLayout = {
//   handle: handle,
//   method: "GetLayout",
//   params: {},
//   outKey: -1,
//   id: 200
// };

// var GetFlipHyperCubeData = {
//   handle: 2,
//   method: "GetHyperCubeData",
//   params: {
//     qPath: "/qHyperCubeDef",
//     qPages: [{ qTop: 0, qLeft: 0, qHeight: 20, qWidth: 20 }]
//   },
//   id: 300
// };

function FlipFlopOpenWS() {
  var OpenDoc = {
    method: "OpenDoc",
    handle: -1,
    params: [
      appid
      //"8e3cb028-88d0-489d-923f-0e611eb7199d"
    ],
    outKey: -1,
    id: 1
  };

  flip_flop_ws = new WebSocket(
    "wss://" +
      config[env].host +
      config[env].prefix +
      "app/" +
      appid +
      "?reloadUri=https://" +
      config[env].host +
      config[env].prefix +
      "dev-hub/engine-api-explorer&v=" +
      new Date()
  );

  flip_flop_ws.onopen = function(msg) {
    // Logic for opened connection
    console.log("Date Range connection opened");
    flip_flop_ws.send(JSON.stringify(OpenDoc));
  };

  flip_flop_ws.onerror = function() {
    console.log("Error!!", "Some issues fetching date range");
  };
}

function LoadFlipGetObjectWS(objid, divId, callback) {
  // id=1

  /*
id = 1   (id*100 + 1)
GETOBJECT = 100
GETLAYOUT = 101
GETHIPERCUBE = 102

*/

  if (!flip_flop_ws) {
    FlipFlopOpenWS();
  }

  var id = ++GetflipObjectIdInitial;
  idInformation[id] = {};
  idInformation[id].divId = divId;
  idInformation[id].handle = 1;

  //GetFlipObject.params.qId = objid;
  //GetFlipObject.id = id * 100;
  GetFlipObject(idInformation[id].handle, id, objid);
  //sendMessage(flip_flop_ws, flip_flop_ws, GetFlipObject);

  flip_flop_ws.onmessage = function(data, flags) {
    var wsres = JSON.parse(data.data);

    if (wsres.id > 3000 && wsres.id < 4000) {
      if (wsres.result) {
        var handle = wsres.result.qReturn.qHandle;
        var id = ++GetflipLayoutIdInitial;
        idInformation[id] = {};
        idInformation[id].divId = idInformation[wsres.id].divId;
        idInformation[id].handle = handle;
        GetFlipLayout(handle, id);
        //flip_flop_ws.send(JSON.stringify(GetDateHyperCubeData));
      } else {
        console.log("Something went wrong with: ", wsres);
      }
    }

    if (wsres.id > 4000 && wsres.id < 5000) {
      var id = ++GetflipHyperCubeIdInitial;
      idInformation[id] = {};
      idInformation[id].header = wsres;
      idInformation[id].divId = idInformation[wsres.id].divId;
      idInformation[id].handle = idInformation[wsres.id].handle;
      GetFlipHyperCubeData(idInformation[id].handle, id);
    } else if (wsres.id > 5000 && wsres.id < 6000 && callback) {
      flipfloplayout.header = idInformation[wsres.id].header;
      flipfloplayout.data = wsres;
      flipfloplayout.id = idInformation[wsres.id].divId;
      callback(flipfloplayout);
    }
  };
}

function GetFlipObject(handle, id, objId) {
  var GetFlipObject = {
    handle: handle,
    method: "GetObject",
    params: {
      qId: objId
    },
    outKey: -1,
    id: id
  };
  sendMessage(flip_flop_ws, JSON.stringify(GetFlipObject));
}

function GetFlipLayout(handle, id) {
  var GetFlipLayout = {
    handle: handle,
    method: "GetLayout",
    params: {},
    outKey: -1,
    id: id
  };
  sendMessage(flip_flop_ws, JSON.stringify(GetFlipLayout));
}
function GetFlipHyperCubeData(handle, id) {
  var GetFlipHyperCubeData = {
    handle: handle,
    method: "GetHyperCubeData",
    params: {
      qPath: "/qHyperCubeDef",
      qPages: [{ qTop: 0, qLeft: 0, qHeight: 10, qWidth: 10 }]
    },
    id: id
  };

  sendMessage(flip_flop_ws, JSON.stringify(GetFlipHyperCubeData));
}
