import React, { Component } from "react";
import InfoIcon from "@material-ui/icons/Info";
import Button from "@material-ui/core/Button";
import Tooltip from "@material-ui/core/Tooltip";

import "./styles.scss";

import { connect } from "react-redux";

class AppSection extends Component {
  constructor(props) {
    super(props);
  }

  render() {
     
    return (
      <div className="col-md-6 padding15">
        <div className="row margin0">
          <div className="col-md-3 col-sm-12 AppDataOne">
            <div className="AppName">
              <span>{this.props.appName}</span>
            </div>
            <div className="AppStream">
              <span>
                {this.props.streamName == null
                  ? "Stream Name"
                  : this.props.streamName.name}
              </span>
            </div>
          </div>
          <div className="col-md-9 AppDataTwo">
            <div className="row">
              <div className="col-md-9 col-sm-12 AppSummary">
                <div className="AppSummaryText">
                  {/* {this.props.description ? this.props.description : "Summary of Application"} */}
                  Description of {this.props.appName}
                </div>
                <div className="AppDate">
                  <div>Published    :{this.props.publish.replace('T',' ').replace('Z',' ')}</div>
                  <div>Last Updated :{this.props.lastUpdate.replace('T',' ').replace('Z',' ')}</div>
                  <div>Last Reload  :{this.props.lastReload.replace('T',' ').replace('Z',' ')}</div>
                </div>
              </div>
              <div className="col-md-3 OpenApp">
                <div className="InfoIconDiv">
                  {/* <InfoIcon /> */}
                  <Tooltip className="tooltipWidth" title="Published:20 Days ago Last Reload:25 Days ago Last Update:18 Days ago" arrow placement="top">
                    <Button className="customWidth"><InfoIcon /></Button>
                  </Tooltip>
                </div>
                <div className="AppRef">
                  <a className="OpenAppButton" onClick={this.props.onClick}>
                    Open App
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default AppSection;
