import React from "react";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import IconButton from "@material-ui/core/IconButton";
import Divider from "@material-ui/core/Divider";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import { withRouter } from "react-router-dom";
// const qlikConfig = require('./../../../assets/configs/config.json');
// import QueueAnim from "rc-queue-anim";
import "./styles1.scss";
import { Button } from "@material-ui/core";

// const LoadExcecHandler = () => {
//     const options = qlikConfig;

//     // console.log('rendered from function')

//     setTimeout(() => {

//       window.LoadCalendar();
//     },500)

//   }

class Calendar extends React.Component {
  state = {
    anchorEl: null,
    bgColor: "grey"
  };

  handleClick = event => {
    this.setState({ anchorEl: event.currentTarget, bgColor: "#0099ff" });
    //console.log("PAGE NAME IS **********",window.pageName);
    setTimeout(() => {
      if(window.pageName == "pipeline"){
       // console.log("INSIDE PIPELINE PAGE");
      window.LoadObject("ytd", "WWPaJG");
      window.LoadObject("comppd", "MnBSUYj");
      window.LoadObject("frmdate1", "DvEQVy");
      window.LoadObject("frmdate2", "DUqdmd");
      window.LoadObject("todate1", "UedZmr");
      window.LoadObject("todate2", "edRWa");
      window.LoadObject("textfield", "GrJMQm");
      }
      else{
        //console.log("INSIDE OTHER PAGES");
      window.LoadObject("ytd", "KjrRma");
      window.LoadObject("comppd", "HPbAMv");
      window.LoadObject("frmdate1", "mNjaJL");
      window.LoadObject("frmdate2", "gnLJTC");
      window.LoadObject("todate1", "VshWek");
      window.LoadObject("todate2", "YTqewWQ");
      window.LoadObject("textfield", "LQUxxUZ");
    }
      // window.LoadObject("month", "JbberK");
      // window.LoadObject("quarter", "BswksZ");
      // window.LoadObject("year", "HazAPT");
      // window.LoadObject("prvyr", "HPbAMv");
      // console.log("not working");
      // window.LoadObject1('calender-div' , 'kBPf')  //samsung calendar
    }, 150);
  };

  handleClose = () => {
    this.setState({ anchorEl: null, bgColor: "grey" });
  };

  render() {
    const { anchorEl } = this.state;

    return (
      <ul className="list-unstyled list-inline">
        <li className="list-inline-item">
          <div>
            <IconButton
              className="header-btn"
              aria-owns={anchorEl ? "header-long-menu" : null}
              aria-haspopup="true"
              onClick={this.handleClick}
              style={{ color: this.state.bgColor }}
              // className="focus"
            >
              <i className="material-icons">date_range</i>
            </IconButton>
            <Menu
              id="header-long-menu"
              anchorEl={anchorEl}
              open={Boolean(anchorEl)}
              onClose={this.handleClose}
            >
              <div style={{ height: "215px", padding: "0px" }}>
                <div
                  style={{
                    height: "30px",
                    width: "100%",
                    marginLeft: "8px",
                    marginBottom: "10px"
                  }}
                >
                  <i
                    class="fa fa-calendar"
                    aria-hidden="true"
                    style={{ fontSize: "30px", color: "#0099ff" }}
                  />
                  <span id="caltitle">Calendar</span>
                </div>
                <div className="row">
                  <div
                    className="col-md-6"
                    style={{ textAlign: "left", paddingLeft: "20px" }}
                  >
                    <span>Period Selected</span>
                  </div>
                  <div
                    className="col-md-6"
                    style={{ textAlign: "right", paddingRight: "20px" }}
                  >
                    <span>Compare To</span>
                  </div>
                </div>
                <div
                  className="row"
                  id="calender-div"
                  style={{ height: "150px", width: "104%" }}
                >
                  <div
                    className="col-sm-9 rgtmenu"
                    style={{ height: "70px" }}
                    id="ytd"
                  />
                  <div
                    className="col-sm-3 rgtmenu"
                    style={{ height: "70px", padding: "2px" }}
                    id="comppd"
                  />
                  <div className="col-sm-1 txtcal">From:</div>
                  <div
                    className="col-sm-2"
                    style={{ height: "35px", paddingTop: "10px" }}
                    id="frmdate1"
                  />
                  <div
                    className="col-sm-2"
                    style={{ height: "35px", paddingTop: "10px" }}
                    id="frmdate2"
                  />
                  <div className="col-sm-2" />
                  <div className="col-sm-1 txtcal">To:</div>
                  <div
                    className="col-sm-2"
                    style={{ height: "35px", paddingTop: "10px" }}
                    id="todate1"
                  />
                  <div
                    className="col-sm-2"
                    style={{ height: "35px", paddingTop: "10px" }}
                    id="todate2"
                  />
                  {/* <div className="col-sm-6" style={{ height: "75px"}} id="fromdate"></div>
                  <div className="col-sm-6" style={{ height: "75px"}} id="todate"></div> */}

                  <div
                    className="col-sm-12"
                    style={{ height: "35px",marginTop:"10px" }}
                    id="textfield"
                  />

                  {/* <div className="col-sm-9 rgtbtn" style={{pointerEvents:"none"}}>
                  <div className="row rgtmenu" style={{ height: "75px"}} id="year"></div>
                  <div className="row rgtmenu" style={{ height: "40px"}} id="quarter"></div>
                  <div className="row rgtmenu" style={{ height: "95px"}} id="month"></div>
                  <div className="row rgtmenu" style={{ height: "40px",marginTop:"2px"}} id="prvyr"></div>
                  </div> */}
                </div>
                {/* <div
                className="row"
                  id="calender-div"
                  style={{ height: "250px", width: "100%" }}
                >
                  <div className="col-sm-3 lftbtn" id="ytd">

                  </div>
                  <div className="col-sm-9 rgtbtn" style={{pointerEvents:"none"}}>
                  <div className="row rgtmenu" style={{ height: "75px"}} id="year"></div>
                  <div className="row rgtmenu" style={{ height: "40px"}} id="quarter"></div>
                  <div className="row rgtmenu" style={{ height: "95px"}} id="month"></div>
                  <div className="row rgtmenu" style={{ height: "40px",marginTop:"2px"}} id="prvyr"></div>
                  </div>
                 
                </div> */}
                {/* </MenuItem> */}
                {/* <MenuItem className="clsmenu"> */}
                {/* <Button className="clsbtn" style={{borderRadius:"8px",zIndex:'1000', top:'14px'}} onClick={this.handleClose}> */}
                {/* <a onClick={this.handleClose} class="close-icon" style={{zIndex:'1000',top:"15%",right:"8%"}}></a> */}
                <i
                  class="fa fa-times-circle-o"
                  style={{
                    zIndex: "1000",
                    top: "2%",
                    right: "2%",
                    position: "absolute",
                    fontSize: "30px",
                    cursor: "pointer"
                  }}
                  aria-hidden="true"
                  onClick={this.handleClose}
                />
                {/* <a onClick={this.handleClose} style={{zIndex:'1000'}} class="close"></a> */}
                {/* <span style={{padding:"0px 16px",fontSize:"25px",fontWeight:"800"}}>x</span> */}
                {/* </Button> */}
              </div>
            </Menu>
          </div>
        </li>
      </ul>
    );
  }
}

// <div className="container-fluid no-breadcrumb page-dashboard">

//   <QueueAnim type="bottom" className="ui-animate">
//     <div>

//     </div>

//     <div key="1"><Main /></div>
//     {LoadExcecHandler()}
//   </QueueAnim>

// </div>

export default withRouter(Calendar);
