import React from "react";
import Avatar from "@material-ui/core/Avatar";
import IconButton from "@material-ui/core/IconButton";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import { withRouter } from "react-router-dom";
import MaterialIcon from "components/MaterialIcon";
import DEMO from "constants/demoData";
import { connect } from "react-redux";
import UserIcon from "./../../../assets/images/user_icon.png";
import { userActions } from "../../../actions/userActions";
import FilterDrawerRight from "./Filters/Filters";
import Calendar from "./Calendar";
import Button from "@material-ui/core/Button";
class NavRightList extends React.Component {
  state = {
    anchorEl: null
  };

  handleClick = event => {
    // console.log( event)
    this.setState({ anchorEl: event.currentTarget });
  };

  handleClose = () => {
    this.setState({ anchorEl: null });
  };

  handleLogout = () => {
    this.props.dispatch(userActions.logout());
  };
  render() {
    const { anchorEl } = this.state;
    //const currUser = JSON.parse(localStorage.getItem("user")).username;
    const { security ,showHeader} = this.props;
     console.log("Props of loaction :",showHeader.showFilter);
    return (
      <ul className="list-unstyled float-right">
        {/* <li className="list-inline-item search-box seach-box-right d-none d-md-inline-block">
          <div className="search-box-inner">
            <div className="search-box-icon"><MaterialIcon icon="search" /></div>
            <input type="text" placeholder="search..." />
            <span className="input-bar"></span>
          </div>
        </li> */}
        {/* {this.props.showHeader.isCaspio ? ( 
        <li>
        <li>
          <h7 style={{ float: "left" }}>
            <div style={{ marginTop: "0px", float: "left" }} />
            <div className="date_range" id="startdate" />
            <div className="date_range2" id="startdate" />
          </h7>
        </li>
      
        <li><Calendar /></li>
        <li><FilterDrawerRight /></li>
        </li>
      ) : (
          ' '
        )}  */}

        {this.props.showHeader.isCaspio ? (
          <li>
            {/* <li>
              <h7 style={{ float: "left" }}>
                <div style={{ marginTop: "0px", float: "left" }} />
                <div className="date_range" id="startdate" />
                <div className="date_range2" id="startdate" />
              </h7>
            </li> */}
            {/* <li class="dropdown qcmd abcd bookmark">
         <IconButton className="drop-toggle clickclass header-btn" data-toggle="dropdown"><i class="material-icons" style={{fontSize:"24px"}}>bookmark_border</i> </IconButton>
        <ul id="qbmlist" class="dropdown-menu mclass"></ul>
        </li> */}

            {/* <li>
              <Calendar />
            </li>
            <li>
              <FilterDrawerRight />
            </li> */}
          </li>
        ) : (
          " "
        )}

          {showHeader.showFilter && (<li><FilterDrawerRight /></li>)}
      
        <li style={{ marginRight: "10px" }}>
          <IconButton
            className="header-btn"
            aria-owns={anchorEl ? "app-header-menu" : null}
            aria-haspopup="true"
            onClick={this.handleClick}
            style={{ color: this.state.bgColor }}
          >
            <Avatar
              alt="avatar"
              src={UserIcon}
              className="rounded-circle header-avatar"
            />
          </IconButton>

          <Menu
            id="app-header-menu"
            className="app-header-dropdown"
            anchorEl={anchorEl}
            open={Boolean(anchorEl)}
            style={{ color: this.state.bgColor }}
            onClose={this.handleClose}
          >
            <MenuItem onClick={this.handleClose}>
              <div>
                <span>Signed in as</span>
                {/* <strong>{currUser}</strong> */}
                <strong>{security.user.username}</strong>
              </div>
            </MenuItem>
            <div className="divider divider-solid my-1" />
            <MenuItem onClick={this.handleClose}>
              <a href="mailto: InView@iris-worldwide.com">
                <i className="material-icons">help</i> <span>Need Help?</span>
              </a>
            </MenuItem>
            <MenuItem>
              <a href="">
                <i className="material-icons">person_outline</i>{" "}
                <span>Change Password</span>
              </a>
            </MenuItem>
            {/* <MenuItem onClick={this.handleLogout}>
              <div>
                <i className="nav-icon material-icons">person_outline</i>
                <span>Add New User</span>
              </div>
            </MenuItem> */}

            {/* <MenuItem onClick={this.handleClose}> <a href="#/app/dashboard"> <i className="material-icons">home</i> <span>Dashboard</span> </a> </MenuItem> */}
            <div className="divider divider-solid my-1" />
            {/* <MenuItem onClick={this.handleClose}> <a href={DEMO.login}> <i className="material-icons">forward</i> <span>Log Out</span> </a> </MenuItem> */}
            <MenuItem onClick={this.handleLogout}>
              <div>
                <i className="material-icons">forward</i> <span>Log Out</span>
              </div>
            </MenuItem>
          </Menu>
        </li>

        {/* <li style={{ marginRight: "10px" }}>
          <IconButton
            className="header-btn"
            aria-owns={anchorEl ? "app-header-menu" : null}
            aria-haspopup="true"
            onClick={this.handleClick}
            style={{ color: this.state.bgColor }}
          >
            <Avatar
              alt="avatar"
              src={UserIcon}
              className="rounded-circle header-avatar"
            />
          </IconButton>

          <Menu
            id="app-header-menu"
            className="app-header-dropdown"
            anchorEl={anchorEl}
            open={Boolean(anchorEl)}
            style={{ color: this.state.bgColor }}
            onClose={this.handleClose}
          >
            <MenuItem onClick={this.handleClose}>
              <div>
                <span>Signed in as</span>
              </div>
            </MenuItem>
            <div className="divider divider-solid my-1" />
            <MenuItem onClick={this.handleClose}>
              <a href="mailto: InView@iris-worldwide.com">
                <i className="material-icons">help</i> <span>Need Help?</span>
              </a>
            </MenuItem>

            <div className="divider divider-solid my-1" />

            <MenuItem onClick={this.handleLogout}>
              <div>
                <i className="material-icons">forward</i> <span>Log Out</span>
              </div>
            </MenuItem>
          </Menu>
        </li>
      */}
      </ul>
    );
  }
}

// export default withRouter(NavRightList);
const mapPropsToState = state => {
  return {
    users: state.users,
    security: state.security,
    errors: state.errors,
    showHeader: state.showHeader
  };
};
export default connect(mapPropsToState)(NavRightList);
