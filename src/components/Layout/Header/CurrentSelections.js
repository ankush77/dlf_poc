import React from "react";
import QueueAnim from "rc-queue-anim";

// import './styles.scss';

// import QdtComponents from 'qdt-components';
import { Typography } from "@material-ui/core";

const qlikConfig = require("./../../../assets/configs/config.json");

// const viz1 = {
//   type: 'QdtCurrentSelections',
//   props: { height: '40px' },
// };
const LoadExcecHandler = () => {
  const options = qlikConfig;

  // // console.log('rendered from function')
  setTimeout(function() {
    // var QdtComponent = new QdtComponents(options.config, options.connections);
    // QdtComponent.render('QdtViz', {id: '59b0ad26-77f3-47b3-b71a-a4584239fb81', height:'300px'}, document.getElementById('qv01'));
    // QdtComponent.render('QdtViz', {id: 'cLMUpA', height:'300px'}, document.getElementById('qv02'));
    // QdtComponent.render('QdtCurrentSelections', {id:'',height:'100px'}, document.getElementById('qv61'));
    // QdtComponent.render(type=viz1,props=this.node);
  }, 500);
};

const Main = () => <div id="qv61" />;

const CurrentSelections = () => (
  <div>
    {/* <Typography variant="display3">AdOverview</Typography> */}
    {/* <div id="qv03"></div> */}
    <QueueAnim type="bottom" className="ui-animate">
      <div />

      <div key="1">
        <Main />
      </div>
      {LoadExcecHandler()}
    </QueueAnim>
  </div>
);

export default CurrentSelections;
