import React from 'react';
import APPCONFIG from 'constants/appConfig';
import DEMO from 'constants/demoData';

class Footer extends React.Component {
  render() {
    return (
      <section className="app-footer">
        <div className="container-fluid">
          <span className="float-left">
            <span>Powered by <a className="brand" target="_blank" href={DEMO.productLink}>{APPCONFIG.Copyright}</a> {APPCONFIG.year}</span>
          </span>
          <span className="float-right">
            <span>Axis <i className="material-icons" style={{lineHeight:"1"}}>favorite_border</i></span>
          </span>
        </div>
      </section>
    );
  }
}

export default Footer;
