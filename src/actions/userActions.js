import setJWTToken from "../securityUnits/setJWTToken";
import Axios from "axios";
import jwt_decode from "jwt-decode";
import { SET_CURRENT_USER } from "../constants/actionTypes";
import endpoints from "../assets/configs/endpoints";
const env = require("../assets/configs/environment").env;
export const userActions = {
  login,
  logout,
  setCurrentUser,
  getHeader,
  register,
  reloadApp,
  lastAppReload,
  appReloadStatus,
  refreshToken,
  getFilter
};

const rootURL = endpoints[env].server_url;

function login(response) {
  return async dispatch => {
    try {
      const res = await Axios.post(rootURL+"/api/users/login", response);
    //   const res = await Axios.post(
    //    "http://localhost:8080/api/users/login",
    //   response
    //  ); 
     const { token } = res.data;
     const { refreshToken } = res.data;
      localStorage.setItem("jwtToken", token);
      localStorage.setItem("refresh", refreshToken);
      //localStorage.setItem("loginFailure", "");
      setJWTToken(token);
      const decodeToken = jwt_decode(token);
      console.log("******QlikTicket**********: ",decodeToken)
      dispatch(success(decodeToken));
    } catch (err) {
      localStorage.setItem("loginFailure", err.response.data.username+" or "+err.response.data.password);
      console.log("Login Failure");
      dispatch(failure(err.response.data));
    }
  };

  function success(user) {
    return {
      type: "LOGIN_SUCCESS",
      payload: user
    };
  }

  function failure(error) {
    return { type: "GET_ERRORS", payload: error };
  }
}

function refreshToken(response) {
  return async dispatch => {
    try {
      //const refreshData = {
        //"refreshTokenId":response
      //}
      localStorage.getItem("jwtToken")

     const res = await Axios.post(rootURL + "/api/users/refresh?refreshTokenId="+response+"&accessTokenId="+localStorage.getItem("jwtToken"));
     // const res = await Axios.post(
       //"http://localhost:8080/api/users/refresh?refreshTokenId="+response+"&accessTokenId="+localStorage.getItem("jwtToken")
      
     //); 
     const { jwt } = res.data;
     console.log("New Token : "+jwt);
      localStorage.setItem("jwtToken", jwt);
      setJWTToken(jwt);
      const decodeToken = jwt_decode(jwt);
      dispatch(success(jwt));
    } catch (err) {
      dispatch(failure(err.response.data));
    }
  };

  function success(user) {
    return {
      type: "REFRESH_TOKEN",
      payload: user
    };
  }

  function failure(error) {
    return { type: "GET_ERRORS", payload: error };
  }
}

function register(response) {
  return async dispatch => {
    try {
      const res = await Axios.post(rootURL + "/api/users/register", response);
     //const res = await Axios.post(
       //"http://localhost:8080/api/users/register",
       //response
     //);
     const reg = res.data;
      dispatch(success(reg));
      console.log("Register Successfully")
    } catch (err) {
      console.log("Register Failure");
      dispatch(failure(err.response.data));
    }
  };

  function success(user) {
    return {
      type: "NEW_USER",
      payload: user
    };
  }

  function failure(error) {
    return { type: "GET_ERRORS", payload: error };
  }
}

function reloadApp(user, dir) {
  return dispatch => {
    (async () => {
      const rawResponse = await fetch(
        "https://axis.in-view.io/api/v1/data/reloadData",
        {
          method: "POST",
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json"
          },
          body: JSON.stringify({ quser: user, qdir: dir })
        }
      );
      const content = await rawResponse.json();
      // var messageId = content.data;
      // console.log("Triggered pub-sub", content);
      dispatch(success(content));
    })();
  };

  function success(content) {
    return { type: "DO_RELOAD", payload: content };
  }

  function failure(errorMessage) {
    return { type: "LOGIN_FAILURE", errorMessage: errorMessage };
  }
}
function lastAppReload(user, dir) {
  return dispatch => {
    (async () => {
      const rawResponse = await fetch("https://axis.in-view.io/api/v1/data/lastReloadTime", {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json"
        },
        body: JSON.stringify({ quser: user, qdir: dir })
      });
      const content = await rawResponse.json();
      // console.log("Last Reload Time response:", content);

      if (content.ok) {
        var reloadTime = content.data;
        dispatch(success(reloadTime));
      } else {
        dispatch(success(failure));
      }
    })();
  };

  function success(reloadTime) {
    return { type: "LAST_RELOAD", reloadTime };
  }

  function failure() {
    return {
      type: "LAST_RELOAD_FAILURE",
      errorMessage: "Something went wrong while fetching app status "
    };
  }
}

function appReloadStatus(status) {
  console.log("from appReloadStatus");
  return dispatch => {
    dispatch(success(status));
  };

  function success(status) {
    return { type: "RELOAD_STATUS", payload: status };
  }
}
function logout() {
  return dispatch => {
    // let user = response;
    localStorage.removeItem("jwtToken");
    localStorage.removeItem("refresh");
    setJWTToken(false);
    dispatch(success());
  };

  function success() {
    return {
      type: SET_CURRENT_USER,
      payload: {}
    };
  }
}

function setCurrentUser(user) {
  return dispatch => {
    // const token = localStorage.getItem("jwtToken");
    // setJWTToken(token);
    // const decodeToken = jwt_decode(token);
    dispatch(success(user));
  };

  function success(user) {
    return {
      type: SET_CURRENT_USER,
      payload: user
    };
  }
}

function getHeader(flag) {
  return dispatch => {
    dispatch(showheader(flag));
  };
  function showheader(flag) {
    return { type: "SHOW_HEADER", payload: flag };
  }
}

function getFilter(flag) {
  return dispatch => {
    dispatch(showFilter(flag));
  };
  function showFilter(flag) {
    return { type: "SHOW_FILTER", payload: flag };
  }
}
