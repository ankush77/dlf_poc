import React from "react";
import QueueAnim from "rc-queue-anim";
import { Grid, Paper } from "@material-ui/core";
import StarBorderIcon from "@material-ui/icons/StarBorder";
import DnsIcon from '@material-ui/icons/Dns';
import AppsIcon from '@material-ui/icons/Apps';
import FormatListBulletedIcon from '@material-ui/icons/FormatListBulleted';
import "./style.scss";
import $ from "jquery";
import Tooltip from "@material-ui/core/Tooltip";
import { Typography } from "@material-ui/core";
import { userActions } from "./../../../../../../actions/userActions";
import { connect } from "react-redux";
import jwt_decode from "jwt-decode";
// const qlikConfig = require("./../../../../../../assets/configs/config.json");
import KPISection from "../../../../../../components/Layout/AppKPI/KPISection"
import AppSection from "../../../../../../components/Layout/AppKPI/AppSection"
import AppGrid from "../../../../../../components/Layout/AppKPI/AppGrid"


class Home extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
        qlikTicket:'',
        datafetched:false,
        appData: [],
        listView: true,
        gridView: false
    };
  }

   componentDidMount() {
    var currInst=this;
    const dataReceived = [];
    if(this.props.qlikScripts.mainjs == 'success'){
      window.LoadDateRange(function(response){
        console.log("Data received:",response.result.qDocList);
         response.result.qDocList.map((data, index) => {
           dataReceived.push(data);
         });
        currInst.setState({appData:dataReceived});
        console.log("DataReceived App :",dataReceived)
      });
         
    }
    
    //  window.location.reload();
  }

  componentWillReceiveProps(nextProps){
    console.log("Inside component will receive props&&&&&&&&&&")
    var currInst=this;
    const dataReceived = [];
    if(nextProps.qlikScripts.mainjs == 'success'){
      window.LoadDateRange(function(response){
        console.log("Data received:",response.result.qDocList);
         response.result.qDocList.map((data, index) => {
           dataReceived.push(data);
         });
        currInst.setState({appData:dataReceived});
        console.log("DataReceived App :",dataReceived)
      });
         
    }

  }


  onOpenAppClick=()=>{
     this.props.history.push('/app/salesforceslicer');
    console.log("Open App clicked!");
  }

  onSelectListView=()=>{
    this.setState({listView:true,gridView:false});

  }
  onSelectGridView=()=>{
    this.setState({listView:false,gridView:true});
  }

  render() {

    console.log("State values:",this.state)
    return (
      <div className="container-fluid no-breadcrumb page-dashboard">
      <KPISection />

       {/* Featured Apps */}
      <div className="col-md-12 col-sm-12">
        <div className="row">
          <div className="featuredApps col-md-10 col-6">
            <StarBorderIcon />
            Featured Apps
          </div>
          <div className="featuredApps col-md-1 pointerCursor col-3 textAlignRight" onClick={this.onSelectListView}><FormatListBulletedIcon/>List</div>
          <div className="featuredApps col-md-1 pointerCursor col-3 textAlignLeft" onClick={this.onSelectGridView}><AppsIcon/>Grid</div>

         </div>
        </div>
          {/* Featured App Ends */}

        {/* Featured Apps Body Starts */}
        <div className="row margin0">
          
              {  
                this.state.appData && this.state.appData.length && (
                 this.state.appData.map((data,index) => {               
                if(this.state.listView){
                  return <AppSection
                  key={index}
                  appName={data.qDocName}
                  streamName={data.qMeta.stream}
                  publish={data.qMeta.publishTime}
                  lastUpdate={data.qMeta.modifiedDate}
                  lastReload={data.qLastReloadTime}
                  description={data.qMeta.description}
                  onClick={this.onOpenAppClick}
                />
                }
                if(this.state.gridView){
                  return <AppGrid
                  key={index}
                  appName={data.qDocName}
                  streamName={data.qMeta.stream}
                  publish={data.qMeta.publishTime}
                  lastUpdate={data.qMeta.modifiedDate}
                  lastReload={data.qLastReloadTime}
                  description={data.qMeta.description}
                  onClick={this.onOpenAppClick}
                />
                }
                
              })
            )
          }
          
        </div>
     
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => ({
  showHeader: state.showHeader,
  users:state.users,
  qlikScripts: state.qlikScripts
});

export default connect(mapStateToProps)(Home);
