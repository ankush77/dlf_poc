import React, { Component } from "react";
import { connect } from "react-redux";
import { Switch } from "react-router-dom";
import classnames from "classnames";
import { MuiThemeProvider } from "@material-ui/core/styles";
import { Route, Redirect } from "react-router-dom";
import loadable from "react-loadable";
import LoadingComponent from "components/Loading";
import setJWTToken from "./../securityUnits/setJWTToken";
import jwt_decode from "jwt-decode";
import QlikScripts from "./../components/qlik-scripts/QlikScripts";

// = styles =
// 3rd
import "styles/bootstrap/bootstrap.scss";
// custom
import "styles/layout.scss";
import "styles/theme.scss";
import "styles/ui.scss";

import lightTheme from "./themes/lightTheme";
import darkTheme from "./themes/darkTheme";
import grayTheme from "./themes/grayTheme";
import { userActions } from "../actions/userActions";
import SecureRoute from "../securityUnits/SecureRoute";
import LoadMashup from "./qlik-scripts/LoadMashup";

let MainApp = loadable({
  loader: () => import("components/Layout/AppLayout/MainApp"),
  loading: LoadingComponent
});
let Exception = loadable({
  loader: () => import("routes/exception/"),
  loading: LoadingComponent
});
let Account = loadable({
  loader: () => import("routes/user/"),
  loading: LoadingComponent
});
let PageFullscreen = loadable({
  loader: () => import("routes/fullscreen/"),
  loading: LoadingComponent
});

class App extends Component {
  componentDidMount() {}

  render() {
    const {
      match,
      location,
      layoutBoxed,
      navCollapsed,
      navBehind,
      fixedHeader,
      sidebarWidth,
      theme,
      security
    } = this.props;
    const isRoot = location.pathname === "/" ? true : false;
    // **************************************************************
    const jwtToken = localStorage.getItem("jwtToken");

    if (jwtToken) {
      if(jwtToken=="Invalid Refresh Token"){
        if (location.pathname !== "/user/login") {
          this.props.dispatch(userActions.logout());
          return <Redirect to={"/user/login"} />;
        }
      }
      else{
        setJWTToken(jwtToken);
        const decoded_jwtToken = jwt_decode(jwtToken);
        if (!security.validToken) {
          this.props.dispatch(userActions.setCurrentUser(decoded_jwtToken));
        }
  
        const currentTime = Date.now() / 1000;
        if (decoded_jwtToken.exp < currentTime) {
          //handle logout
          if (location.pathname !== "/user/login") {
            this.props.dispatch(userActions.logout());
            return <Redirect to={"/user/login"} />;
          }
          //const refreshToken = localStorage.getItem("refresh");
          //this.props.dispatch(userActions.refreshToken(refreshToken));
        }
      }
     
    }

    if (isRoot && !security.validToken) {
      return <Redirect to={"/user/login"} />;
    }
    // **************************************************************

    let materialUITheme;
    switch (theme) {
      case "gray":
        materialUITheme = grayTheme;
        break;
      case "dark":
        materialUITheme = darkTheme;
        break;
      default:
        materialUITheme = lightTheme;
    }

   

    if (security.validToken) {
      if (
        location.pathname !== "/app/dashboard" &&
        (isRoot || location.pathname == "/user/login")
      ) {
       
         return <Redirect to={"/app/dashboard"} />;
      }
    }
    // else {
    //   if (location.pathname != "/user/login") {
    //     return <Redirect to={"/user/login"} />;
    //   }
    // }

    if(location.pathname == "/app/salesforceslicer"){
      this.props.dispatch(userActions.getFilter(true));
    }
    else{
      this.props.dispatch(userActions.getFilter(false));
     }



    return (
      <MuiThemeProvider theme={materialUITheme}>
        <div id="app-inner">
          <div className="preloaderbar hide">
            <span className="bar" />
          </div>
          <div
            className={classnames("app-main h-100", {
              "fixed-header": fixedHeader,
              "nav-collapsed": navCollapsed,
              "nav-behind": navBehind,
              "layout-boxed": layoutBoxed,
              "theme-gray": theme === "gray",
              "theme-dark": theme === "dark",
              "sidebar-sm": sidebarWidth === "small",
              "sidebar-lg": sidebarWidth === "large"
            })}
          >
            {this.props.security.validToken &&
            this.props.security.user.qsToken ? (
              <QlikScripts ticket={this.props.security.user.qsToken} />
            ) : (
              ""
            )}

            {this.props.qlikScripts.requirejs === "success" ? (
              <LoadMashup />
            ) : (
              ""
            )}
            <Switch>
              <SecureRoute path={`${match.url}app`} component={MainApp} />
              {/* <Route path={`${match.url}exception`} component={Exception} /> */}
              <Route path={`${match.url}user`} component={Account} />
              <Route path={`*`} component={Exception} />
              <SecureRoute
                exact
                path="/fullscreen"
                component={PageFullscreen}
              />
            </Switch>
          </div>
        </div>
      </MuiThemeProvider>
    );
  }
}

const mapStateToProps = (state, ownProps) => ({
  layoutBoxed: state.settings.layoutBoxed,
  navCollapsed: state.settings.navCollapsed,
  navBehind: state.settings.navBehind,
  fixedHeader: state.settings.fixedHeader,
  sidebarWidth: state.settings.sidebarWidth,
  theme: state.settings.theme,
  users: state.users,
  security: state.security,
  qlikScripts: state.qlikScripts
});

export default connect(mapStateToProps)(App);
