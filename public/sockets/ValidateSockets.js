// Wait until the state of the socket is not ready and send the message when it is...
function sendMessage(socket, msg) {
  waitForSocketConnection(socket, function() {
    console.log("message sent!!!");
    socket.send(msg);
  });
}

//Wait for Socket to be opened
function waitForSocketConnection(socket, callback) {
  setTimeout(function() {
    if (socket && socket.readyState === 1) {
      console.log("Connection is made");
      if (callback != null) {
        callback();
      }
    } else {
      console.log("wait for connection...");
      waitForSocketConnection(socket, callback);
    }
  }, 200); // wait 500 milisecond for the connection...
}
