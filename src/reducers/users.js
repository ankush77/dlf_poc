import {
  LOGIN_SUCCESS,
  LOGIN_FAILURE,
  LOGOUT,
  DO_RELOAD,
  LAST_RELOAD,
  RELOAD_STATUS,
  LAST_RELOAD_FAILURE
} from "./../constants/actionTypes";
import { StateChangeTypes } from "downshift";

const initialSettings = {
  isLoggedin: false,
  loginFailure: "",
  user: {},
  reloadApp: {},
  lastAppReload: null,
  appReloadStatus: false,
  appReloadStatusErr: "",
  newToken:""
};
const users = (state = initialSettings, action) => {
  switch (action.type) {
    case LOGIN_SUCCESS:
      return {
        ...state,
        isLoggedin: action.isLoggedin,
        user: action.payload
      };
      case "REFRESH_TOKEN":
      return {
        ...state,
        newToken: action.payload
      };
    case LOGIN_FAILURE:
      return {
        ...state,
        isLoggedin: false,
        loginFailure: action.errorMessage
      };
    case LOGOUT:
      return {
        ...state,
        isLoggedin: false,
        user: {}
      };

    case DO_RELOAD:
      return {
        ...state,
        reloadApp: {
          lastReload: action.payload.reloadTime,
          messageId: action.payload.messageId
        },
        appReloadStatus: false
      };
    case LAST_RELOAD:
      return {
        ...state,
        lastAppReload: action.reloadTime
      };
    case LAST_RELOAD_FAILURE:
      return {
        ...state,
        appReloadStatusErr: action.payload
      };
    case RELOAD_STATUS:
      return {
        ...state,
        appReloadStatus: action.payload
      };
    default: {
      return state;
    }
  }
};

export default users;
