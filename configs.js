var configs = {
  ssl: {
    devlocal: {
      key: "ssl/localhost/localhost.key",
      crt: "ssl/localhost/localhost.crt"
    },
    dev: {
      key: "ssl/in-view.io/in-view.io.key",
      crt: "ssl/in-view.io/in-view.io.crt"
    },
    test: {
      key: "ssl/in-view.io/in-view.io.key",
      crt: "ssl/in-view.io/in-view.io.crt"
    },
    preprod: {
      key: "ssl/in-view.io/in-view.io.key",
      crt: "ssl/in-view.io/in-view.io.crt"
    },
    prod: {
      key: "ssl/in-view.io/in-view.io.key",
      crt: "ssl/in-view.io/in-view.io.crt"
    }
  },
  appPort: 7000,

  qlik: {
    devlocal: {
      qs_host: "qlikdemo.polestarllp.com",
      qs_vproxy: "virproxy",
      qs_rest_api_port: "4243",
      qs_port: 443,
      qs_isSecure: true
    },
    dev: {
      qs_host: "dev.instantview.io",
      qs_vproxy: "axis-dev",
      qs_rest_api_port: "4243",
      qs_port: 443,
      qs_isSecure: true
    },
    test: {
      qs_host: "dev.instantview.io",
      qs_vproxy: "axis-test",
      qs_rest_api_port: "4243",
      qs_port: 443,
      qs_isSecure: true
    },
    prod: {
      qs_host: "qsprod.in-view.io",
      qs_vproxy: "axis",
      qs_rest_api_port: "4243",
      qs_port: 443,
      qs_isSecure: true
    }
  },
  db: {
    devlocal: {
      user: "2db899bd2062706520",
      password: "2d80c784207f6c46394cf35fa2b244da",
      host: "55de80de70242d3e671daa5fa0b6",
      db: "25bee4af00495143"
    },
    dev: {
      user: "2db899bd2062706520",
      password: "2d80c784207f6c46394cf35fa2b244da",
      host: "55de80de70242d3e671daa5fa0b6",
      db: "25bee4af00495143"
    },
    test: {
      user: "2db899bd2062706520",
      password: "2d80c784207f6c46394cf35fa2b244da",
      host: "55de80de70242d3e671daa5fa0b6",
      db: "25bee4af00495143"
    },
    prod: {
      user: "0d98999d2062706520",
      password: "5683f1da275a6d776a0ddd34c8ce01",
      host: "57da9ac7773f29237e1eb3",
      db: "25bee4af00495143"
    }
  }
};

module.exports = configs;
