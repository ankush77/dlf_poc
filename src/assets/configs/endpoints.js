const env = {
  devlocal: {
     //server_url: "http://localhost:8080",
    //server_url:"http://10.0.0.22:8080/dlf",
   server_url:"http://qlikdemo.polestarllp.com:8081/dlf",
    qsconfigs: {
      baseUrl: "https://qlikdemo.polestarllp.com/virproxy/resources",
      prefix: "/virproxy/",
      port: 443,
      host: "qlikdemo.polestarllp.com",
      isSecure: true,
      config: {
        text: {
          useXhr: function(url, protocol, hostname, port) {
            // enable xhr load for all text resources
            return true;
          },
          createXhr: function() {
            // use withCredentials flag to prevent authentication errors
            var xhr = new XMLHttpRequest();
            xhr.withCredentials = true;
            return xhr;
          }
        }
      }
    },
    appid: "6e01a888-55e5-4d72-9562-49e6d0e0781f"
  },
  dev: {
    server_url: "https://samsung-dev.in-view.io/webapi",
    qsconfigs: {
      baseUrl: "https://dev.instantview.io/inview-dev/resources",
      prefix: "/inview-dev/",
      port: 443,
      host: "dev.instantview.io",
      isSecure: true,
      config: {
        text: {
          useXhr: function(url, protocol, hostname, port) {
            // enable xhr load for all text resources
            return true;
          },
          createXhr: function() {
            // use withCredentials flag to prevent authentication errors
            var xhr = new XMLHttpRequest();
            xhr.withCredentials = true;
            return xhr;
          }
        }
      }
    },
    appid: "d844740d-84f9-403c-ba40-3d828e492404"
  },
  test: {
    server_url: "https://samsung-test.in-view.io/webapi",
    qsconfigs: {
      baseUrl: "https://dev.instantview.io/inview-test/resources",
      prefix: "/inview-test/",
      port: 443,
      host: "dev.instantview.io",
      isSecure: true,
      config: {
        text: {
          useXhr: function(url, protocol, hostname, port) {
            // enable xhr load for all text resources
            return true;
          },
          createXhr: function() {
            // use withCredentials flag to prevent authentication errors
            var xhr = new XMLHttpRequest();
            xhr.withCredentials = true;
            return xhr;
          }
        }
      }
    },
    appid: "0fd1f6f6-c1ac-4025-9551-e198b4a9c816"
  },

  prod: {
    server_url: "https://samsung-ptp.in-view.io/webapi",
    qsconfigs: {
      baseUrl: "https://qsprod.in-view.io/inview/resources",
      prefix: "/inview/",
      port: 443,
      host: "qsprod.in-view.io",
      isSecure: true,
      config: {
        text: {
          useXhr: function(url, protocol, hostname, port) {
            // enable xhr load for all text resources
            return true;
          },
          createXhr: function() {
            // use withCredentials flag to prevent authentication errors
            var xhr = new XMLHttpRequest();
            xhr.withCredentials = true;
            return xhr;
          }
        }
      }
    },
    appid: "92b5a859-0fc0-4a79-8b4c-e8c9e157e341"
  }
};
export default env;
