import {
  SHOW_HEADER,SHOW_FILTER
} from '../constants/actionTypes';

const initialSettings = {
  isCaspio: false,
  showFilter:false
}
const caspiopage = (state = initialSettings, action) => {

  switch (action.type) {
    case SHOW_HEADER:
      return {
        ...state,
        isCaspio: action.payload,
      };
    case SHOW_FILTER:
      return {
        ...state,
        showFilter: action.payload,
      };
    default:
      return state;
  }
}
export default caspiopage;
