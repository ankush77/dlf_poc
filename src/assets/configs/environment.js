/*Environments Values

    ->  Dev Local     -- 'devlocal'
    ->  Dev     -- 'dev'
    ->  Test    -- 'test'
    ->  Preprod -- 'preprod'
    ->  Prod    -- 'prod'

*/
//testing
var Environments = {
  env: "devlocal"
};

module.exports = Environments;
